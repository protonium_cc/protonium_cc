

from subprocess import call

c_mult = "1S0"

ncol = 2

n_quad = 10

diag_mod = 0

nintervals = [(200, 800, 200)]
#nintervals = [(2, 2, 2)]

rmax_pp = 600.0
rmax_mm = 20.0
rmax_nn = 30.0
alpha_pp = 1.05
rc = 1.20
llambda = 1.10
#E0_r = -0.012
#E0_i = -0.0006
E0_r = -12.126
E0_i = -0.614

rstep_gamma = 0.1
rmax_gamma = 3.0

rc_0 = rc
rc_1 = rc

llambda_0 = llambda
llambda_1 = llambda

fwf="./data/wf_1S0:NIJ_rc_1200_l_110.dat"

flog = "./data/p_pbar_1S0_NIJ_rc_{0}_Lambda{1}_{2}_test.log".format(rc, llambda, rmax_pp)

for ni in nintervals:
    print "nintervals_pp, nintervals_mm, nintervals_nn: ", ni[0], ni[1], ni[2]
    str = "./x_protonium_cc > {17} << end \n&INPUT c_mult='{0}', nintervals_pp={1}, nintervals_mm={2}, nintervals_nn={3} rmax_pp={4}, rmax_mm={5}, rmax_nn={6} rc_0={7}, rc_1={8} lambda_0={9}, lambda_1={10}, ncol={11}, E0_r = {12}, E0_i={13}, n_quad={14}, diag_mod={15}, alpha_pp={16}, rstep_gamma={18}, rmax_gamma={19} / \nend".format(c_mult, ni[0], ni[1], ni[2], rmax_pp, rmax_mm, rmax_nn, rc_0, rc_1, llambda_0, llambda_1, ncol, E0_r, E0_i, n_quad, diag_mod, alpha_pp, flog, rstep_gamma, rmax_gamma)
    args = []
    args.append(str)
    call(args, shell = True)

#str = "cp wf.dat {0}".format(fwf)
#args = []
#args.append(str)
#call(args, shell = True)

str = "cat {0}".format(flog)
args = []
args.append(str)
call(args, shell = True)

str = "mv -f fort.11 {0}".format("contrib_gamma_ic_0.dat")
args = []
args.append(str)
call(args, shell = True)

str = "mv -f fort.12 {0}".format("contrib_gamma_ic_1.dat")
args = []
args.append(str)
call(args, shell = True)

str = "mv -f fort.13 {0}".format("contrib_gamma_ic_2.dat")
args = []
args.append(str)
call(args, shell = True)

str = "mv -f fort.14 {0}".format("contrib_gamma_ic_3.dat")
args = []
args.append(str)
call(args, shell = True)
