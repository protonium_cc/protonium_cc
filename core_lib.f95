!Library of mathematical functions and subroutines
!E.Ydrefors Apr-2010

module core_lib

  integer, parameter::DBL=kind(1.0d0)
  real(DBL), parameter::pi=3.14159265358979323846d0

contains


  pure function jhat(j) result(res)
    !Evaluates sqrt(j+1) for the given integer
    !j: integer=2*angular momentum

    integer, intent(in)::j 
    real(DBL)::res
    res=sqrt(j+1.0d0)
  end function jhat



  pure function dblfac(n) result(d)
    !Evaluates the double factorial of an odd number= (2n-1)!!
    !n: integer>=1

    integer, intent(in)::n
    integer::k
    real(DBL)::d
   ! if(n<1) STOP "Error the argument has to a positive number"
    d=1.0d0
    do k=1, 2*n-1, 2
       d=d*k
    end do
  end function dblfac


  function fac(n) result(f)
    !Computes the factorial of the given integer
    !n: positive integer

    integer, intent(in)::n
    integer:: k
    real(DBL)::f
    if(n<0) then
       write(0,*) n
       STOP "ERROR the argument has to be a non-negative number"
    end if
    f=1.0d0
    do k=1, n
       f=f*k
    end do
  end function fac


  function gamma_half_int(k) result(g)
    !Evaluates gamma(k+1/2) where k is an integer
    !k: positive integer
    integer, intent(in)::k
    real(DBL)::g
    if(k<0) then
       STOP "Error the argument has to be a non-negative number"
    elseif(k==0) then
       g=sqrt(pi)
    else
       g=(1.0d0/2)**k*dblfac(k)*sqrt(pi)
    end if
  end function gamma_half_int

  
  function bcoeff(n,k) result(res) 
    !Evaluates the binomial coefficient ( n )
    !                                   ( k )
    !n, k integers n>=k

    integer, intent(in)::n, k
    integer::m
    real(DBL)::res

    res=0.0d0

    do m=1, n
       res=res+log(m+0.0d0)
    end do

    do m=1, k
       res=res-log(m+0.0d0)
    end do

    do m=1, n-k
       res=res-log(m+0.0d0)
    end do

    res=exp(res)

  end function bcoeff
    


  function binomcoeff(n,k) result(res)
    !Evaluates the binomial coefficient (n+1/2), 
    !                                   ( k   ) 
    !n,k: integers n>=k                               

    integer, intent(in)::n, k
    real(DBL)::sum1, sum2, logRes, res
    integer:: j
    if(k>n) STOP "Illegal choice of arguments: They have to satisfy k<=n"
    sum1=0.0d0
    do j=0,k-1
       sum1=sum1+log(real(2*(n+1)-2*j-1,DBL))
    end do

    sum2=0.0d0
    do j=1, k
       sum2=sum2+log(real(j,DBL))
    end do

    logRes=-k*log(2.0d0)+sum1-sum2
    res=exp(logRes)
  end function binomcoeff


  function binomcoeff_2(n,k) result(res)
    !Evaluates the binomial coefficient ((n+k-1)/2)
    !                                    (n      )

    !n,k: integers, n>=0, k>=0, k<=n

    integer, intent(in)::n, k
    real(DBL)::res
    integer::j, m
    integer::l

    if(n==0) then
       res=1.0d0
    else
       res=0.0d0
       j=n+k-1
       l=0
       do m=0, n-1
          if((j-2*m)<0) then
             l=l+1
          end if
       end do
       do m=0, n-1
          res=res+log(real(abs(j-2*m),DBL))
       end do
       res=exp(res)/(2**n*fac(n))*phase(l)
    end if

  end function binomcoeff_2
          

  pure function phase(n) result(res)
    !Calculates  (-1)^n for an integer n
    !n: integer

    integer, intent(in)::n
    integer::res
    if(mod(n,2)==0) then
       res=1
    else
       res=-1
    end if
  end function phase


  function lambda(kappa,n,l) result(res)
    !Calculates Lambda_kappa(nl), 
    !i.e coefficients for the associated Laguerre polynomials
    !n, l, kappa: non-negative integers satisfying n-kappa>=0

    integer, intent(in)::kappa, n, l
    real(DBL)::res
    if(min(kappa,n,l)<0 .or. (n-kappa)<0) STOP "Error bad arguments to lambda"
    res=phase(kappa)/real(fac(kappa),DBL)*binomcoeff(n+l,n-kappa)
  end function lambda



  function cont_frac(a,b,n,eps,isign,status) result(f_n)
    !Calculates the value of the continued fraction by using modified Lent'z method
    !a: coefficients a_1, a_2, ...,a_{n-1}
    !b: coefficents b_0, b_1, ..., b_{n-1}
    !eps: required accuracy
    !isign: keeps track of the sign of the denominator
    !status: 0=>Succesful computation, /=0: error
    !returns: f_n=b0+a_1(b_1+)*a_2/(b_2+)...

    integer, intent(in)::n
    real(DBL), dimension(n), intent(in)::b
    real(DBL), dimension(n-1), intent(in)::a
    real(DBL), intent(in)::eps
    integer, intent(out)::status
    integer, intent(in out)::isign
    real(DBL)::f_n, c_n, d_n, delta_n
    integer::j

    if(abs(b(1))<1.0d-30) then
       f_n=1.0d-30
    else 
       f_n=b(1)
    end if
    c_n=f_n
    d_n=0.0d0
    
    status=-1
    isign=1
    do j=1, n-1
       d_n=b(j+1)+a(j)*d_n
       if (abs(d_n)<1.0d-30) then
          d_n=1.0d-30
       end if

       c_n=b(j+1)+a(j)/c_n

       if (abs(c_n)<1.0d-30) then
          c_n=1.0d-30
       end if
       d_n=1/d_n

       delta_n=c_n*d_n

       f_n=f_n*delta_n

       if(d_n<0.0) isign=-isign
       if(abs((delta_n-1))< eps) then
          status=0
          exit
       end if

    end do

  end function cont_frac

    function ccont_frac(a,b,n,eps,status) result(f_n)
    !Calculates the value of the continued fraction by using modified Lent'z method. Same as cont_frac but for complex numbers
    !a: coefficients a_1, a_2, ...,a_{n-1}
    !b: coefficents b_0, b_1, ..., b_{n-1}
    !eps: required accuracy
    !status: 0=>Succesful computation, /=0: error
    !returns: f_n=b0+a_1(b_1+)*a_2/(b_2+)...

    integer, intent(in)::n
    complex(DBL), dimension(n), intent(in)::b
    complex(DBL), dimension(n-1), intent(in)::a
    real(DBL), intent(in)::eps
    integer, intent(out)::status
    complex(DBL)::f_n, c_n, d_n, delta_n
    integer::j

    if(abs(b(1))<1.0d-30) then
       f_n=1.0d-30
    else 
       f_n=b(1)
    end if
    c_n=f_n
    d_n=0.0d0
    
    status=-1
    do j=1, n-1
       d_n=b(j+1)+a(j)*d_n
       if (abs(d_n)<1.0d-30) then
          d_n=1.0d-30
       end if

       c_n=b(j+1)+a(j)/c_n

       if (abs(c_n)<1.0d-30) then
          c_n=1.0d-30
       end if
       d_n=1/d_n

       delta_n=c_n*d_n

       f_n=f_n*delta_n

       if(abs((delta_n-1))< eps) then
          status=0
          exit
       end if

    end do

  end function ccont_frac



end module core_lib
