module potential_regularization

  !Regularization parameters
  integer :: MCUT = 3 !Type of regularization
  integer :: NCUT = 1 !The power n in the regularization function

end module potential_regularization

module physical_constants

  real(8), parameter ::  pi = 3.14159265358979323846d0
  real(8), parameter ::  hbarc = 197.327d0       !\hbar*c
  real(8), parameter ::  alpha = 0.0072973531d0  !fine-structure constant
  real(8), parameter ::  M_N = 938.919d0         !nucleon mass 
!  real(8), parameter ::  a0 = 57.63980733d0      !Bohr radius  
  real(8), parameter :: a0 = 1.0d0
  real(8), parameter ::  mp = 938.2796d0         !proton mass 
  real(8), parameter ::  mn = 939.5654d0         !neutron mass 
  real(8), parameter ::  mm = 763.0d0            !meson mass
  
end module physical_constants


module pot

  integer, parameter, private:: DBL = 8

contains

  subroutine pot_isospin(nc, S, L, J, rc_0, rc_1, lambda_0, lambda_1, &
       ra, r, Delta, dm, icoul, iobe, V)
    !---------------------------------------------------------------------
    !PURPOSE:
    !         Computes the regularized potential for all radial points
    !         in proton-neutron basis
    !INPUT:
    !nc:      number of components for the partial wave
    !S: spin of 1st component (proton)
    !L: orbital angular momentum of 1st component (proton)
    !J: total angular momentum of 1st component (proton)
    !rc_0: cut-off radius for T=0
    !rc_1: cut-off radius for T=1
    !lambda_0: strength of annihilation potential for T = 0
    !lambda_1: strength of annihilation potential for T = 1
    !ra: annihilation radius
    !r: radial mesh point
    !Delta: meson-proton mass difference, i.e. m_p-m_m
    !dm : proton-neutron mass difference, m_n-m_p
    !icoul : 0 => no coulomb interaction, otherwise included
    !icoul : 0=> no obe (strong) interaction, otherwise included
    !OUTPUT:
    !------------------------------------------------------------------------------
    !V: Potential
    !------------------------------------------------------------------------------

    use potential_regularization, only: MCUT, NCUT
    use physical_constants, only: alpha, hbarc
    
    integer, intent(in) :: nc, S, L, J, icoul, iobe
    real(DBL), intent(in) :: ra, lambda_0, lambda_1, rc_0, rc_1,  r, Delta, dm
    real(DBL), dimension(nc, nc), intent(out) :: V
    
    !local variables

    integer :: T, i
    real(DBL) :: V_0, V_1,  x, Va_0, Va_1, V_c

    
    
    V = 0.0d0

    V_0 = 0.0d0
    V_1 = 0.0d0
    V_c = 0.0d0

    if(iobe /= 0) then
    
       T = 0
    
       call V_NN_BP_reg(T, S, J, L, L, NCUT, MCUT, rc_0, r, V_0)
    
       T = 1
    
       call V_NN_BP_reg(T, S, J, L, L, NCUT, MCUT, rc_1, r, V_1)    

    end if
    
    x = r/ra

    Va_0 = -lambda_0*hbarc*exp(-x)/r

    Va_1 = -lambda_1*hbarc*exp(-x)/r


    if(icoul /= 0) then
     
       V_c = -hbarc*alpha/r

    end if
       
!    V_c = 0.0d0

!    V_1 = V_0
    
    if(nc == 1) then

       V(1, 1) = 0.5d0*(V_0 + V_1) + V_c  

    end if

    if(nc == 2) then

       V(1, 1) = 0.5d0*(V_0 + V_1) + V_c  

       V(2, 1) = 0.5d0*(Va_0 + Va_1)

       V(1, 2) = V(2, 1)

       V(2, 2) = -2*Delta

    end if
    
    if(nc == 4) then       
       
       V(1, 1) = 0.5d0*(V_0 + V_1) -hbarc*alpha/r  

       V(2, 1) = 0.5d0*(Va_0 + Va_1)

       V(3, 1) = 0.5d0*(V_0 - V_1)

       V(4, 1) = 0.5d0*(Va_0 - Va_1)

       V(1, 2) = V(2, 1)

       V(2, 2) = -2*Delta

       V(3, 2) = 0.5d0*(Va_0 - Va_1)

       V(4, 2) = 0.0d0

       V(1, 3) = V(3, 1)

       V(2, 3) = V(3, 2)

       V(3, 3) = 0.5d0*(V_0 + V_1) + 2*dm

       V(4, 3) = 0.5d0*(Va_0 + Va_1)

       V(1, 4) = V(4, 1)

       V(2, 4) = V(4, 2)

       V(3, 4) = V(4, 3)

       V(4, 4) = -2*Delta
   
    end if

  end subroutine pot_isospin

    subroutine V_NN_BP_reg(T, S, J, L, LP, n, MCUT, r_c, r, V)
   !-----------------------------------------------------------
   !PURPOSE:
   !        Implementation of the N-NB B-P potential with
   !        regularization
   !INPUT:
   !T: Isopin (0 or 1)
   !S: spin
   !L: angular momentum of final state
   !LP: angular momentum of initial state
   !n: power of the polynomial
   !MCUT: kind of regularization
   !     0 -> V=0 for r <= r_c
   !     1 -> V=const for r < = r_c
   !     2 -> V linear for r< = r_c and V(0) = 0
   !     3 -> V=\beta*x^n + \gamma*x^{n+1} for r <= r_c
   !     4 -> V=\beta*x^n+\gamma*x^{n+1} +\delta*x^{n+2}
   !r_c: cut-off
   !r: distance from origin
   !OUTPUT:
   !V: value of potential at r
   !-------------------------------------------------------------

   integer, intent(in) :: T, S, J, L, LP, n, MCUT
   real(DBL), intent(in) :: r_c, r
   real(DBL), intent(out) :: V
 
   !constants
   real(DBL), parameter :: eps = 1.0d-4

   !local variables
   real(DBL) :: x, V_c, V_c_p, V_c_m, V_pc, beta, gamma, delta, V_ppc

   x = r/r_c

   V = 0.0d0
   
   if(x > 1.0d0) then

      call V_NN_BP(T, S, J, L, LP, r, V)

   else

      call V_NN_BP(T, S, J, L, LP, r_c, V_c)   !potential at r=r_c

      if(MCUT == 0) then

         V = 0.0d0

      else if(MCUT == 1) then

         V = V_c

      else if(MCUT == 2) then

         V = x*V_c

      else if(MCUT == 3) then

         call V_NN_BP(T, S, J, L, LP, r_c + eps, V_c_p)

         call V_NN_BP(T, S, J, L, LP, r_c - eps, V_c_m)

         V_pc = (V_c_p - V_c_m)/(2*eps)  !V'(r_c)

         beta = (n+1.0d0)*V_c - V_pc*r_c

         gamma = V_pc*r_c -n*v_c

         V = (beta+gamma*x)*x**n

      else if(MCUT == 4) then

         call V_NN_BP(T, S, J, L, LP, r_c + eps, V_c_p)
         
         call V_NN_BP(T, S, J, L, LP, r_c - eps, V_c_m)

         V_pc = r_c*(V_c_p - V_c_m)/(2*eps)  !V'(x=1)
         V_ppc = r_c*r_c*(V_c_m + V_c_p - 2.0d0*V_c)/(eps*eps)

         beta = 0.5d0*(V_ppc - 2*(n+1)*V_pc + (n+1)*(n+2)*V_c)
         gamma = -V_ppc + (2*n+1)*V_pc - n*(n+2)*V_c
         delta = 0.5d0*(V_ppc - 2*n*V_pc + n*(n+1)*V_c)

         V = (beta + gamma*x + delta*x*x)*x**N

      end if

   end if

 end subroutine V_NN_BP_reg



 subroutine V_NN_NIJ_reg(T, S, J, L, LP, n, MCUT, r_c, r, V)
   !-----------------------------------------------------------
   !PURPOSE:
   !        Implementation of the N-NB Nijmegen potential with
   !        regularization
   !INPUT:
   !T: Isopin (0 or 1)
   !S: spin
   !L: angular momentum of final state
   !LP: angular momentum of initial state
   !n: power of the polynomial
   !MCUT: kind of regularization
   !     0 -> V=0 for r <= r_c
   !     1 -> V=const for r < = r_c
   !     2 -> V linear for r< = r_c and V(0) = 0
   !     3 -> V=\beta*x^n + \gamma*x^{n+1} for r <= r_c
   !     4 -> V=\beta*x^n+\gamma*x^{n+1} +\delta*x^{n+2}
   !r_c: cut-off
   !r: distance from origin
   !OUTPUT:
   !V: value of potential at r
   !-------------------------------------------------------------

   integer, intent(in) :: T, S, J, L, LP, n, MCUT
   real(DBL), intent(in) :: r_c, r
   real(DBL), intent(out) :: V
 
   !constants
   real(DBL), parameter :: eps = 1.0d-4

   !local variables
   real(DBL) :: x, V_c, V_c_p, V_c_m, V_pc, beta, gamma, delta, V_ppc

   x = r/r_c

   V = 0.0d0
   
   if(x > 1.0d0) then

      call V_NN_NIJ(T, S, J, L, LP, r, V)

   else

      call V_NN_NIJ(T, S, J, L, LP, r_c, V_c)   !potential at r=r_c

      if(MCUT == 0) then

         V = 0.0d0

      else if(MCUT == 1) then

         V = V_c

      else if(MCUT == 2) then

         V = x*V_c

      else if(MCUT == 3) then

         call V_NN_NIJ(T, S, J, L, LP, r_c + eps, V_c_p)

         call V_NN_NIJ(T, S, J, L, LP, r_c - eps, V_c_m)

         V_pc = (V_c_p - V_c_m)/(2*eps)  !V'(r_c)

         beta = (n+1.0d0)*V_c - V_pc*r_c

         gamma = V_pc*r_c -n*v_c

         V = (beta+gamma*x)*x**n

      else if(MCUT == 4) then

         call V_NN_NIJ(T, S, J, L, LP, r_c + eps, V_c_p)
         
         call V_NN_NIJ(T, S, J, L, LP, r_c - eps, V_c_m)

         V_pc = r_c*(V_c_p - V_c_m)/(2*eps)  !V'(x=1)
         V_ppc = r_c*r_c*(V_c_m + V_c_p - 2.0d0*V_c)/(eps*eps)

         beta = 0.5d0*(V_ppc - 2*(n+1)*V_pc + (n+1)*(n+2)*V_c)
         gamma = -V_ppc + (2*n+1)*V_pc - n*(n+2)*V_c
         delta = 0.5d0*(V_ppc - 2*n*V_pc + n*(n+1)*V_c)

         V = (beta + gamma*x + delta*x*x)*x**N

      end if

   end if

 end subroutine V_NN_NIJ_reg

 subroutine V_NN_NIJ(T, S, J, L, LP, r, V)
   !------------------------------------------------------------
   !PURPOSE:
   !        Implementation of the Nijmegen potential
   !        P. H. Timmers, W. A. van der Sanden, J. J de Swart,
   !        Phys. Rev. D 29 (1984) 1928 (16). and
   !        M. M. Nagels, T. A. Rijken, J. J. de Swart,
   !        Phys. Rev. D 12 (1975) 744.
   !INPUT:
   !T: Isopin (0 or 1)
   !S: spin
   !L: angular momentum of final state
   !LP: angular momentum of initial state
   !r: distance from origin
   !OUTPUT:
   !V: value of potential at r
   !-------------------------------------------------------------

   use physical_constants, only: hbarc, M_N

   integer, intent(in) :: T, S, J, L, LP
   real(DBL), intent(in) :: r
   real(DBL), intent(out) :: V

   !Constants

   !Pseudo-scalars (\pi, \eta, X^0)
   integer, parameter :: n_ps = 3 !Number of pseudo scalars
   integer, parameter, dimension(n_ps) :: iso_ps = (/ 1, 0, 0 /)  !iso-spin
   real(DBL), dimension(n_ps), parameter :: m_ps = (/ 138.041d0, 548.8d0, 957.5d0 /)  !masses
   real(DBL), dimension(n_ps), parameter :: g_ps = (/ 3.66d0, 2.72967d0, 3.88786d0 /) !couplings

   
   !Vectors (\rho,  \omega, \phi)

   integer, parameter :: n_v = 3  !Number of vectors 
   integer, parameter, dimension(n_v) :: iso_v = (/ 1, 0, 0 /)  !iso-spin
   real(DBL), dimension(n_v), parameter :: m_v = (/ 770.0d0, 783.9d0, 1019.5d0 /)  !masses
   real(DBL), dimension(n_v), parameter :: g_v = (/ 0.59444d0, 3.37308d0, -1.12412d0 /) !couplings
   real(DBL), dimension(n_v), parameter :: fg_v = (/ 4.81696d0, 2.33992d0, -0.51004d0 /) !f/g

   !Scalars (\epsilon)
   integer, parameter :: n_s = 1 !Number of  scalars
   integer, parameter, dimension(n_s) :: iso_s = (/ 0 /)  !iso-spin
   real(DBL), dimension(n_s), parameter :: m_s = (/ 760.0d0 /)  !masses
   real(DBL), dimension(n_s), parameter :: g_s = (/ 5.03208d0 /) !couplings

   !Parameters for two-poles approxmation of \epsilon and \rho
   integer, parameter :: n_b = 2
   real(DBL), parameter, dimension(n_b) :: m_1 = (/ 508.58d0, 628.74d0 /)
   real(DBL), parameter, dimension(n_b) :: m_2 = (/ 1043.79d0, 878.18d0 /)
   real(DBL), parameter, dimension(n_b) :: beta_1 = (/ 0.19986d0, 0.15874d0 /)
   real(DBL), parameter, dimension(n_b) :: beta_2 = (/ 0.55241d0, 0.78321d0 /)

      
   !local variables

   integer :: t1_t2, s1_s2, f_iso, t_c, i
   real(DBL) :: s12_me, so_me, y, x, b, r1, r2, r3, r4, g, g2, qls_me, x1, x2
   real(DBL), dimension(n_ps) :: V_ps
   real(DBL), dimension(n_v) :: V_v
   real(DBL), dimension(n_s) :: V_s
   real(DBL), dimension(n_b) :: V_b
   
   
   V = 0.0d0

   t1_t2 = 2*T*(T+1) - 3      !matrix element of \tau_1*\tau_2
   s1_s2 = 2*S*(S+1) - 3      !matrix element of \sigma_1*\sigma_2
   s12_me = S_12(S, J, L, LP)       !matrix element of S_12
   so_me = spin_orbit(S, J, L, LP)  !matrix element of L*S
   qls_me = quadrupole(S, J, L, LP)
   t_c = 1
   
   
   if(L /= LP) then

      t_c = 0
      s1_s2 = 0

   end if

   
   !iso-scalar exchanges

   do i = 1, n_ps
   
      f_iso = 1 + iso_ps(i)*(-1 + t1_t2)
      y = (m_ps(i)/(2*M_N))**2
      x = m_ps(i)*r/hbarc
      V = y*(s1_s2/3.0d0 + F_T(x)*s12_me)
      g2 = g_ps(i)**2
      V_ps(i) = f_iso*g2*m_ps(i)*V*yukawa(x)

   end do


   !vector exchanges

   do i = 1, n_v

      f_iso = 1 + iso_v(i)*(-1 + t1_t2)
      b = fg_v(i)
      y = (m_v(i)/(2*M_N))**2
      x = m_v(i)*r/hbarc
      g = g_v(i)
      g2 = g*g
      r1 = (1.0d0 + 0.5d0*y)*g2 + 2*y*b*g + y**2*b**2      
      r2 = y*g2 + 2*y*b*g + y*(1 + 0.5d0*y)*b**2
      r3 = 2*y*(3*g2 + 4*b*g + 3*y*b**2)
      r4 = y**2*(g2 + 8*b*g + 8*b**2) 
      V = r1*t_c + r2*(2.0d0/3.0d0*s1_s2 - F_T(x)*s12_me) - r3*F_LS(x)*so_me + r4*FQ(x)*qls_me
      
      V_v(i) = f_iso*m_v(i)*V*yukawa(x)

   end do
      

   !Scalar exchanges

   do i = 1, n_s

      f_iso = 1 + iso_s(i)*(-1 + t1_t2)
      y = (m_s(i)/(2*M_N))**2
      x = m_s(i)*r/hbarc
      g = g_s(i)
      V = (0.5d0*y - 1)*t_c - 2*y*F_LS(x)*so_me - y**2*FQ(x)*qls_me
      V_s(i) = f_iso*g**2*m_s(i)*V*yukawa(x)

   end do

   !Treatment of broad resonances

   V_b = 0.0d0

   if(L == LP) then
   
      do i = 1, n_b
         
         x1 = m_1(i)*r/hbarc
         
         x2 = m_2(i)*r/hbarc

         V_b(i) = m_1(i)*beta_1(i)*yukawa(x1) + m_2(i)*beta_2(i)*yukawa(x2)
         
      end do

   end if
      
   V = - V_ps(1) + V_ps(2) + v_ps(3) + V_v(1) - V_v(2) - V_v(3) + V_s(1) + V_b(1) + V_b(2)  
   
   
 end subroutine V_NN_NIJ

 
      
 subroutine V_NN_BP(T, S, J, L, LP, r, V)
   !------------------------------------------------------------
   !PURPOSE:
   !        Implementation of the N-NB Bryan-Phillips potential
   !        R. A. Bryan and R. J. N Phillips
   !        NPB 5 (1968) 201
   !INPUT:
   !T: Isopin (0 or 1)
   !S: spin
   !L: angular momentum of final state
   !LP: angular momentum of initial state
   !r: distance from origin
   !OUTPUT:
   !V: value of potential at r
   !-------------------------------------------------------------

   use physical_constants, only: hbarc, M_N

   integer, intent(in) :: T, S, J, L, LP
   real(DBL), intent(in) :: r
   real(DBL), intent(out) :: V

   !Constants

   !Pseudo-scalars (\pi, \eta)
   integer, parameter :: n_ps = 2 !Number of pseudo scalars
   integer, parameter, dimension(n_ps) :: iso_ps = (/ 1, 0 /)  !iso-spin
   real(DBL), dimension(n_ps), parameter :: m_ps = (/ 138.2d0, 548.0d0 /)  !masses
   real(DBL), dimension(n_ps), parameter :: g2_ps = (/ 13.5d0, 7.0d0 /) !couplings

   
   !Vectors (\rho, \omega)

   integer, parameter :: n_v = 2  !Number of vectors (\rho, \omega)
   integer, parameter, dimension(n_v) :: iso_v = (/ 1, 0 /)  !iso-spin
   real(DBL), dimension(n_v), parameter :: m_v = (/ 760.0d0, 782.0d0 /)  !masses
   real(DBL), dimension(n_v), parameter :: g2_v = (/ 0.68d0, 21.5d0 /) !couplings
   real(DBL), dimension(n_v), parameter :: fg_v = (/ 4.4d0, 0.0d0 /) !f/g

   !Scalars (\sigma_1, \sigma_2)
   integer, parameter :: n_s = 2 !Number of  scalars
   integer, parameter, dimension(n_s) :: iso_s = (/ 0, 1 /)  !iso-spin
   real(DBL), dimension(n_ps), parameter :: m_s = (/ 560.0d0, 770.0d0 /)  !masses
   real(DBL), dimension(n_ps), parameter :: g2_s = (/ 9.4d0, 6.1d0 /) !couplings

      
   !local variables

   integer :: t1_t2, s1_s2, f_iso, t_c, i
   real(DBL) :: s12_me, so_me, y, x, b, r1, r2, r3
   real(DBL), dimension(n_ps) :: V_ps
   real(DBL), dimension(n_v) :: V_v
   real(DBL), dimension(n_s) :: V_s
   
   V = 0.0d0

   t1_t2 = 2*T*(T+1) - 3      !matrix element of \tau_1*\tau_2
   s1_s2 = 2*S*(S+1) - 3      !matrix element of \sigma_1*\sigma_2
   s12_me = S_12(S, J, L, LP)       !matrix element of S_12
   so_me = spin_orbit(S, J, L, LP)  !matrix element of L*S
   t_c = 1
   
   
   if(L /= LP) then

      t_c = 0
      s1_s2 = 0

   end if

   
   !iso-scalar exchanges

   do i = 1, n_ps
   
      f_iso = 1 + iso_ps(i)*(-1 + t1_t2)
      y = (m_ps(i)/(2*M_N))**2
      x = m_ps(i)*r/hbarc
      V = y*(s1_s2/3.0d0 + F_T(x)*s12_me)
      V_ps(i) = f_iso*g2_ps(i)*m_ps(i)*V*yukawa(x)

   end do


   !vector exchanges

   do i = 1, n_v

      f_iso = 1 + iso_v(i)*(-1 + t1_t2)
      b = fg_v(i)
      y = (m_v(i)/(2*M_N))**2
      x = m_v(i)*r/hbarc
      r1 = (1.0d0 + y*(1.0d0+b))**2      !Eq. (9)
      r2 = y*((1.0d0+b)**2)/3.0d0              !Eq. (10)
      r3 = 2*y*(3.0d0+4.0d0*b+3.0d0*y*b*b) !(12)
      V = r1*t_c + r2*(2.0d0*s1_s2-3.0d0*F_T(x)*s12_me) - r3*F_LS(x)*so_me
      V_v(i) = f_iso*g2_v(i)*m_v(i)*V*yukawa(x)

   end do
      

   !Scalar exchanges

   do i = 1, n_s

      f_iso = 1 + iso_s(i)*(-1 + t1_t2)
      y = (m_s(i)/(2*M_N))**2
      x = m_s(i)*r/hbarc
      V = (y-1.0d0)*(t_c+2*y*F_LS(x)*so_me)
      V_s(i) = f_iso*g2_s(i)*m_s(i)*V*yukawa(x)

   end do
  

   V = - V_ps(1) + V_ps(2) + V_v(1) - V_v(2) + V_s(1) - V_s(2) 
   
   
 end subroutine V_NN_BP


 

 elemental function yukawa(x) result(y)
   !--------------------------------------
   !PURPOSE:
   !        Computes the value exp(-x)/x
   !-------------------------------------

   real(DBL), intent(in) :: x
   real(DBL) :: y

   y = exp(-x)/x

 end function yukawa

 elemental function F_T(x) result(f)
   !----------------------------------------
   !PURPOSE:
   !       Computes F_T(x), i.e 1/3*H(x)/F(X)
   !       apperaring in the BP interaction
   !INPUT:
   !x: value of x
   !-------------------------------------------

   real(DBL), intent(in) :: x
   real(DBL):: f

   f = 1.0d0/3.0d0+1/x+1/(x*x)

 end function F_T

 elemental function F_LS(x) result(f)
   !-------------------------------------------
   !PURPOSE:
   !        Computes the radial function
   !        multiplying the LS term in th BP
   !        interaction
   !INPUT:
   !x: value of x
   !------------------------------------------
   
   real(DBL), intent(in) :: x
   real(DBL) :: f

   f = (x+1)/(x*x)

 end function F_LS
   
 elemental function FQ(x) result(f)
   !-------------------------------------------
   !PURPOSE:
   !        Computes the radial function
   !        multiplying the quadrupole term in the
   !        Nijmegen interaction
   !INPUT:
   !x: value of x
   !------------------------------------------

   real(DBL), intent(in) :: x
   real(DBL) :: f

   
   f = (x*(x + 3) + 3)/(x**4)

 end function FQ
 
 
 
 elemental function S_12(S, J, L, LP) result(S12)
   !-------------------------------------------------------------
   !PURPOSE:
   !       Computes the  tensor interaction matrix element, S_12
   !INPUT:
   !S: spin
   !J: total angular momentum
   !L: Orbital angular momentum of final state
   !LP: Orbital angular momentum of intitial state
   !--------------------------------------------------------------
   
   integer, intent(in) :: S, J, L, LP
   real(DBL) :: S12

   !local variables
   integer :: JM1, JP1
   
   S12 = 0.0d0

   if(S /= 1) return
   
   JM1 = J-1
   JP1 = J+1


   if( L == LP) then

      if(L == JM1) then

         S12 = -2.0d0*JM1/(2*J+1.0d0) 

      else if(L == J) then

         S12 = 2.0d0

      else if(L == JP1) then

         S12 = -2.0d0*(J+2.0d0)/(2*J+1.0d0)
            
      end if

   else if((L == JM1 .and. LP == JP1) .or. (L == JP1 .and. LP == JM1)) then

      S12 = 6.0d0*sqrt(J*(J+1.0d0))/(2*J+1.0d0)

   end if
   
 end function S_12
 

 elemental function spin_orbit(S, J, L, LP) result(SO)
   !----------------------------------------------------------------------
   !PURPOSE:
   !        Computes the matrix element of the spin-orbit operator L\cdot S
   !INPUT:
   !S: spin
   !J: total angular momentum
   !L: Orbital angular momentum of final state
   !LP: Orbital angular momentum of intitial state
   !-----------------------------------------------------------------------

   integer, intent(in) :: S, J, L, LP
   real(DBL) :: SO

   SO = 0.0d0

   if(L /= LP .or. L < 1 .or. S < 1) return
   
 
   if(J <= (L+S) .and. J >= abs(L-S)) then

      SO = 0.5d0*(J*(J+1)-L*(L+1)-S*(S+1))

   end if
 

 end function spin_orbit

  elemental function quadrupole(S, J, L, LP) result(Q12)
   !----------------------------------------------------------------------
   !PURPOSE:
   !        Computes the matrix element of the quadrupole spin-orbit operator
   !INPUT:
   !S: spin
   !J: total angular momentum
   !L: Orbital angular momentum of final state
   !LP: Orbital angular momentum of intitial state
   !-----------------------------------------------------------------------

   integer, intent(in) :: S, J, L, LP
   real(DBL) :: Q12

   Q12 = 0.0d0

   if((L /= LP) .or. (L == 0) .or. ((J > (L + S) .or. J < abs(L - S)))) return
   
   if(S == 0) then

      Q12 = - J*(J + 1.0d0)

   else if(L == J - 1) then

      Q12 = (J - 1.0d0)**2

   else if(L == J) then

      Q12 = 1.0d0 - J*(J + 1.0d0)


   else if(L == J + 1) then

      Q12 = (J + 2.0d0)**2

   end if

   
 end function quadrupole


end module pot
