
module potential_regularization

  !Regularization parameters
  integer :: MCUT = 3 !Type of regularization
  integer :: NCUT = 1 !The power n in the regularization function

end module potential_regularization

module physical_constants

  real(8), parameter ::  pi = 3.14159265358979323846d0
  real(8), parameter ::  hbarc = 197.327d0       !\hbar*c
  real(8), parameter ::  alpha = 0.0072973531d0  !fine-structure constant
  real(8), parameter ::  M_N = 938.919d0         !nucleon mass 
!  real(8), parameter ::  a0 = 57.63980733d0      !Bohr radius  
  real(8), parameter :: a0 = 1.0d0
  real(8), parameter ::  mp = 938.2796d0         !proton mass 
  real(8), parameter ::  mn = 939.5654d0         !neutron mass 
  real(8), parameter ::  mm = 763.0d0            !meson mass

  real(8), parameter ::  a_0 = 57.63980733d0      !Bohr radius  

  
end module physical_constants

module bstate_cc_mod

  integer, parameter :: DBL = 8
  
  private

  integer, parameter :: VMODEL = 1     !0 => BP, 1 => Nijmegen 

  public :: solve_cc
  
contains

  

  subroutine solve_cc(c_mult, nintervals_pp, nintervals_mm, nintervals_nn, &
       xmax_pp, xmax_mm, xmax_nn, alpha_pp, rc_0, &
       rc_1, lambda_0, lambda_1, ncol, E0, n_quad, diag_mod, verbose, ibc, &
       rstep_gamma, rmax_gamma)
    !---------------------------------------------------------------------------------------------
    !PURPOSE :
    !       Solves the bound-state coupled-channel SE
    !INPUT :
    !c_mult : string labeling the multipole
    !nintervals_pp : number of subintervals to be used for  p\bar{p} channel
    !nintervals_mm : number of subintervals to be used for  meson channels
    !nintervals_nn : number of subintervals to be used for  n\bar{n} channel
    !xmax_pp : maximum value r, in units of a0, for p\bar{p} channel
    !xmax_mm : cut-off for meson channels
    !xmax_nn : cut-off for n-\bar{n} channel
    !alpha_pp : scaling parameter for p\bar{p} channel
    !rc_0: cut-off radius for T=0
    !rc_1: cut-off radius for T=1
    !lambda_0: strength of annihilation potential for T = 0
    !lambda_1: strength of annihilation potential for T = 1
    !ncol : 2 => cubic splines, 3 => quintic splines
    !E0 : initial energy
    !n_quad : number of quadrature points for integration
    !diag_mod : diagonalization method ; 0 =>, /= 0 iterative solution
    !verbose : > 0 => print matrices
    !ibc : boundary condition at r=rmax : 0 -> logarithmic derivative
    !                                     1 -> u(rmax) = 0
    !rstep_gamma : step size for tabulation of annihilation distribution
    !rmax_gamma : maximum of r for tabulation of annihilation distribution
    !-------------------------------------------------------------------------------------------

    use omp_lib, only : omp_get_wtime
    
    use spline_mod_cubic_quintic, only : S_x, Spp_x, get_xlimits, Stilde_x, Stilde_pp_x, &
         colloc_gauss, sub_interv_equidist, xpoints, xmesh, nintervals_array => nx_array

    use physical_constants, only : hbarc, a0, mp, mn, mm, a_0

    
    character(len=6), intent(in) :: c_mult
    integer, intent(in) :: nintervals_pp, nintervals_mm, nintervals_nn, ncol, &
         n_quad, diag_mod, verbose, ibc
    real(DBL), intent(in) :: xmax_pp, xmax_mm, xmax_nn, alpha_pp, rc_0, rc_1, lambda_0, lambda_1, &
         rstep_gamma, rmax_gamma
    complex(DBL), intent(in) :: E0
    !local variables
    integer :: nc, npoints, ndim, ix, idx, ic, jc, irow, icol, info, n_phys, &
         nintervals, nsplines, nrows, ncols, nmult 
    real(DBL) :: ra, mu, a, b, mu_1, xp, r, diff, t1, t2, m1, Delta, dm, xmax, E_c, res, rstep, rmax
    real(DBL), dimension(0:7) :: m_array
    complex(DBL) :: k2, u, upp, E, q, norm, H_ev
    integer, dimension(0:7) :: npoints_array
    integer, dimension(:), allocatable :: Lvec, Svec, Jvec
    real(DBL), dimension(0:7) :: xmax_array, alpha_array
    complex(DBL), dimension(:), allocatable :: ld, qvec
    real(DBL), dimension(:), allocatable :: mu_array
    complex(DBL), dimension(:, :), allocatable :: P, H
    real(DBL), dimension(:, :), allocatable :: V 
    complex(DBL), dimension(:), allocatable :: energies, e_phys, coeff
    complex(DBL), dimension(:, :), allocatable :: evec   !eigenvectors
    complex(DBL), dimension(:, :), allocatable :: contributions

    if(ncol /= 2 .and. ncol /= 3 ) then
       write(0, *) 'Error in solve_cc: Invalid value of n_col'
       stop
    end if

    
    E_c = 0.0d0
    
    select case (c_mult)
    
    case("1S0")

       nc = 4

       nmult = 1

       allocate(Lvec(0:(nmult - 1)), Svec(0:(nmult - 1)), Jvec(0:(nmult - 1)))
       
       Lvec = 0

       Svec = 0

       Jvec = 0
       
       E_c = -hbarc**2*1.0d3/(a_0**2*mp)

    
       print *, "E_c: ", E_c, "keV"
       
    case("3S1")

       nc = 4

       nmult = 1

       allocate(Lvec(0:(nmult - 1)), Svec(0:(nmult - 1)), Jvec(0:(nmult - 1)))
       
       Lvec = 0

       Svec = 1

       Jvec = 1
       
       E_c = -hbarc**2*1.0d3/(a_0**2*mp)

    
       print *, "E_c: ", E_c, "keV"

    case("3D1")

       nc = 4

       nmult = 1

       allocate(Lvec(0:(nmult - 1)), Svec(0:(nmult - 1)), Jvec(0:(nmult - 1)))
       
       Lvec = 2

       Svec = 1

       Jvec = 1
       
       E_c = -hbarc**2*1.0d3/(a_0**2*mp)

    
       print *, "E_c: ", E_c, "keV"

       
       

    case("3SD1")

       nc = 8

       nmult = 2
       
       allocate(Lvec(0:(nmult - 1)), Svec(0:(nmult - 1)), Jvec(0:(nmult - 1)))
       
       Lvec(0:1) = (/ 0, 2 /)

       Svec(0:1) = (/ 1, 1 /)

       Jvec(0:1) = (/ 1,  1 /)

       E_c = -hbarc**2*1.0d3/(a_0**2*mp)

    
       print *, "E_c: ", E_c, "keV"

       
    case default
       return
    end select

    
    
    E_c = 0.0d0
   
    
    Delta = mp - mm

    dm = mn - mp

    m_array = (/ mp, mm, mn, mm, mp, mm, mn, mm /)

    m1 = m_array(0)

    allocate(mu_array(0:(nc-1)), qvec(0:(nc-1)), ld(0:(nc-1)))

    do ic = 0, nc - 1

       mu_array(ic) = m_array(ic)*a0*a0/hbarc**2

    end do
       
    mu_1 = mu_array(0)
    
    ra = hbarc/m1 !Annihilation radius


     
  

    allocate(nintervals_array(0:7))
    
    nintervals_array = (/ nintervals_pp, nintervals_mm, nintervals_nn, nintervals_mm, &
         nintervals_pp, nintervals_mm, nintervals_nn, nintervals_mm /)
 

    do ic = 0, nc - 1

       npoints_array(ic) = ncol*(nintervals_array(ic) + 1) - 2

    end do


    xmax_array = (/ xmax_pp, xmax_mm, xmax_nn, xmax_mm, xmax_pp, xmax_mm, xmax_nn, xmax_mm /)

    do ic = 0, nc - 1

       k2 = m_array(ic)*a0*a0*(E0*1.0d-3 + 2*abs(m1-m_array(ic)))/(hbarc*hbarc)

 !      k2 = m_array(ic)*a0*a0*(E0*1.0d-3 + 2*(m1-m_array(ic)))/(hbarc*hbarc)
       
       q = sqrt(k2)

       if((ic == 0 .or. ic == 4 ) .and. real(q, DBL) > 0.0d0 )  q = -q

       qvec(ic) = q

       ld(ic) = compute_ld(q, xmax_array(ic), ic, lvec(ic/(nc/nmult)))


       print *, "ic, , l, ld, q: ", ic, lvec(ic/(nc/nmult)), ld(ic), qvec(ic)
       
    end do

    
    
    ndim = sum(npoints_array(0:(nc-1)))

    
    npoints = maxval(npoints_array)
    nintervals = maxval(nintervals_array)
    
    allocate(xpoints(0:(nc-1), 0:(npoints-1)), P(ndim, ndim), H(ndim, ndim), &
         xmesh(0:(nc-1), 0:(nintervals)), V(nc, nc), contributions(0:(nc-1), 0:(nc-1)))

    alpha_array = 1.0d0

    alpha_array(0) = alpha_pp

    alpha_array(4) = alpha_pp
    
  !  alpha_array(0:(nc - 1)) = alpha_pp
    
     
    do ic = 0, nc - 1

       nintervals = nintervals_array(ic)
       npoints = npoints_array(ic)
       xmax = xmax_array(ic)
    
       call sub_interv_equidist(0.0d0, xmax, xmesh(ic, 0:nintervals), alpha_array(ic))

       print *
       
       print *, "ic, xmesh: ", ic, xmesh(ic, 0:nintervals)
       
       call colloc_gauss(ncol, xmesh(ic, 0:nintervals), xpoints(ic, 0:(npoints-1)))

       print *
       
!       print *, "ic, xpoints: ", ic, xpoints(ic, 0:(npoints-1)) 

       
    end do

    
    
!    write(0, *) "xmesh: ", xmesh

!    write(0, *) "xpoints: ", xpoints

    
!    stop

    print *, "ibc: ", ibc
    
    P = 0.0d0

    t1 = omp_get_wtime()


    print *, "nintervals_array:" , nintervals_array

    
    nrows = 0
    
    do ic = 0, nc-1

       npoints = npoints_array(ic)
       nintervals = nintervals_array(ic)

       do idx = 0, npoints - 1

          do ix = 0, npoints - 1

             
             if(ibc == 0 .and. idx == npoints-1) then

                u = Stilde_x(ncol, ic, idx, xpoints(ic, ix), ld(ic))

                
             else

                u = S_x(ncol, ic, idx, xpoints(ic, ix))
                
                
             end if

             P(nrows + ix + 1 , nrows + idx + 1) = u

          end do

       end do

       nrows = nrows + npoints
       
    end do

    P = 1.0d-3*P

  
    
!    stop
    
    t2 = omp_get_wtime()

!    print *, "time to compute P: ", t2-t1

    H = 0.0d0

    nrows = 0
    ncols = 0

    t1 = omp_get_wtime()


    
    do ic = 0, nc-1

       ncols = 0
       
       do jc = 0, nc-1
       
          npoints = npoints_array(ic)
          nsplines = npoints_array(jc)
       
          do idx = 0, nsplines - 1

             call get_xlimits(ncol, jc, idx, a, b)
          
             do ix = 0, npoints - 1

                xp = xpoints(ic, ix)
          
                r = a0*xp

                
                if( xp >= a .and. xp <= b ) then

                   call  pot_isospin(nc, nmult, Svec, Lvec, Jvec, rc_0, rc_1, lambda_0, lambda_1, &
                        ra, r, Delta, dm, E_c, V)
            
            
                   u = S_x(ncol, jc, idx, xp)
                   upp = Spp_x(ncol, jc, idx, xp)
               

                   ! mu = mu_array(ic)/mu_1
                   mu = mu_1/mu_array(ic)

                         
                   irow = nrows + ix + 1
                   icol = ncols  + idx + 1
           !        icol = idx + 1 + jc*npoints
                  
                   H(irow, icol) = mu_1*V(ic+1, jc+1)*u
                  
                   if(ic == jc) then
                      H(irow, icol)  = H(irow, icol) + &
                           mu*(-upp + Lvec(ic/(nc/nmult))*(Lvec(ic/(nc/nmult))+1)/(xp**2)*u) 
                   end if
                   
                   
                end if
                
             end do

          end do

          ncols = ncols + npoints_array(jc)
          
       end do

       nrows = nrows + npoints_array(ic)
       
    end do

    
    
    t2 = omp_get_wtime()

    print *, "time to compute H (no bc): ", t2-t1

    
    
    t1 = omp_get_wtime()


    !Implement the boundary condition at r=r_max using logarithmic derivatives

    if(ibc == 0) then

    !   jc = 1

       nrows = 0
       ncols = 0


       do ic = 0, nc-1

          ncols = 0
          
          do jc = 0, nc-1    

          
             npoints = npoints_array(ic)
             nsplines = npoints_array(jc)
       
             idx = nsplines-1
       
             call get_xlimits(ncol, jc, idx, a, b)
       
             do ix = 0, npoints - 1

                xp = xpoints(ic, ix)

                r = xp*a0
          
                if( xp >= a .and. xp <= b ) then

                   call  pot_isospin(nc, nmult, Svec, Lvec, Jvec, rc_0, rc_1, lambda_0, lambda_1, &
                        ra, r, Delta, dm, E_c, V)
                   !             V = 0.0d0

            
           
               
                
                   !                mu = mu_array(ic)/mu_1

                   mu = mu_1/mu_array(ic)
                
          
                                   
                   irow = nrows + ix + 1
                   icol = ncols + idx + 1

                   
                u = Stilde_x(ncol, jc, idx, xp, ld(jc))
                upp = Stilde_pp_x(ncol, jc, idx, xp, ld(jc))
          
                   
                !   u = Stilde_x(ncol, idx, xpoints(ix), ld(jc))
                 !  upp = Stilde_pp_x(ncol, idx, xpoints(ix), ld(jc))

                   
                H(irow, icol) = mu_1*V(ic+1, jc+1)*u


                   
                   if(ic == jc) then
                      H(irow, icol)  = H(irow, icol) + &
                           mu*(-upp + Lvec(ic/(nc/nmult))*(Lvec(ic/(nc/nmult))+1)/(xp**2)*u) 
                   end if

                end if
                
             end do

             ncols = ncols + npoints_array(jc)
             
          end do

          nrows = nrows + npoints_array(ic)
          
       end do
       
    end if

!    stop
    
    t2 = omp_get_wtime()
    
    print *, "time to implement bc: ", t2-t1

  

    E = E0

    if(verbose > 0) then

    
       !print the matrices

       open(unit = 10, file ='matrices.dat')

    
       write(10, *) 'P: '
       
       do ix = 0, ndim - 1

          write(10, *) P(ix + 1, :)
          
       end do


       
       write(10, *) 'H: '
       
       do ix = 0, ndim - 1

          write(10, *) H(ix + 1, :)
          
       end do


!       write(10, *) 'X: '
       
!       do ix = 0, ndim - 1

!          write(10, *) X(ix + 1, :)
          
!       end do

       
       
      close(10)

    end if

    t1 = omp_get_wtime()

    print *, "npoints_array: ", npoints_array(0:(nc - 1))

    print *, "ndim: ", ndim

    
  
    
    if(diag_mod == 0) then

       allocate(energies(ndim), evec(ndim, ndim), e_phys(ndim), coeff(ndim))

   !    allocate(evec(ndim, ndim))
       
     !  stop
       
       call diag_zgeev(P, H, ndim, energies, evec, info)
       
    else
       
       allocate(energies(1), evec(ndim, 1), e_phys(1), coeff(ndim))

      
       energies(1) = E0*mu_array(0)
       
       call diag_rq_new(P, H, ndim, energies(1), evec(:, 1), info)           
       
    end if
       
    t2 = omp_get_wtime()

    

    print *, "time to diagonalize: ", t2-t1

  

    n_phys = 0
    
    do ix = 1, size(energies, 1)
       if(real(E_c*mu_array(0)+energies(ix), DBL) < 0.0d0) then
             
          n_phys = n_phys + 1
             
          e_phys(n_phys) =  1.0d0/(mu_array(0))*energies(ix)

       end if
          
    end do

    idx = 1

    diff = 100.0d0

    print *, "e_phys: ", e_phys(:n_phys)
    
    do ix = 1, size(energies, 1)
       
       if(abs(E_c+e_phys(ix)-E0) < diff) then
          diff = abs(E_c+e_phys(ix)-E0)
          idx = ix
       end if
                     
    end do
    
    
    print *
    print *, "=========================================================="
    print *, "OUTPUT: "
    print *     
    print *, "energies for first channel (E_1): "

    print *, real(e_phys(idx), DBL), aimag(e_phys(idx))
  !  print "(a, f5.3, a)", "dE: ", 1.0d0*(12.49107/1.0d0+real(e_phys(idx), DBL)), " keV"
    print "(a, f5.3, a)", "dE: ", 1.0d0*(real(e_phys(idx), DBL)), " keV"
    print "(a, f5.3, a)", "Gamma: ", -2*aimag(e_phys(idx))*1.0d0, " keV"
    print *
    print *, "=========================================================="



    !Tabulate wave function

    coeff = evec(:, idx)


    
    norm = compute_norm(coeff, ncol, nc, 2, ld, ndim)

    norm = sqrt(norm)
    
    print *, "Normalization: ", norm

    coeff = coeff/norm

 !   write(100, *) "coeff : ", coeff

    
    H_ev = compute_H_ev(coeff, ncol, nc, nmult,  2, rc_0, rc_1, lambda_0, lambda_1, ra, Svec, Lvec, Jvec, Delta, &
         dm, ld, mu_array, ndim, E_c, contributions)
    
    print *, "H_ev: ", H_ev*1.0d3

    print *

    
    print "(a)", "=================================================================="
    
    print "(a)", "Contributions :"

    
    print "(a, 5x, a, 5x, a, 5x, a)", "i_i", "i_f", "E_R", "E_I"

    print "(a)", "-------------------------------------------------------------------"

    
    do jc = 0, nc - 1

       do ic = 0, nc - 1

          print "(i1,7x,i1, 2x, e9.3, 2x, e9.3)", jc, ic, 1.0d3*real(contributions(ic, jc), DBL), 1.0d3*aimag(contributions(ic, jc))

       end do

    end do


    print "(a)", "=================================================================="

    ic = 0
    
    open(unit = 10, file ='wf.dat')

    call tabulate_wf(10, ic, coeff, ncol, nc, ld, ndim, 0.1d0, 500.0d0)

    
    close(10)


    open(unit = 10, file ='wf2.dat')

    ic = 1
   
    call tabulate_wf(10, ic, coeff, ncol, nc, ld, ndim, 0.1d0, 10.0d0)

    
    close(10)

    if(verbose > 0) then
    
    open(unit = 10, file ='d2wf2.dat')

    call tabulate_2nd_deriv_wf(10, ic, coeff, ncol, nc, ld, ndim, 0.1d0, 10.0d0)

    close(10)
    
    open(unit = 10, file ='prod.dat')

    call tabulate_wf_2nd_deriv_wf(10, ic, coeff, ncol, nc, ld, ndim, 0.1d0, 3.0d0)

    close(10)

    open(unit = 10, file ='prod_all.dat')

    call tabulate_wf_2nd_deriv_wf_all(10, coeff, ncol, nc, ld, ndim, 0.1d0, 3.0d0, mu_array)

    close(10)
    
    open(unit = 10, file ='gamma.dat')
    
    call tabulate_gamma(10, coeff, ncol, nc, ld, mu_array, ndim, rstep_gamma, rmax_gamma)

    close(10)

    end if
    
 !   ic = 0

  !  print *, compute_width_ic(ic, coeff, ncol, nc, rc_0, rc_1, lambda_0, lambda_1, ra, S, &


    !          L, J, Delta, dm, ld, mu_array, ndim)

    norm = 0.0d0
    
    do ic = 0, nc - 1

!       print *, "ic: ", ic
       print *, ic, &
            compute_norm_ic(ic, coeff, ncol, nc, 2, ld, ndim)

       norm = norm + compute_norm_ic(ic, coeff, ncol, nc, 2, ld, ndim)
       
!       res = compute_width_ic(ic, coeff, ncol, nc, rc_0, rc_1, lambda_0, lambda_1, ra, S, &
 !           L, J, Delta, dm, ld, mu_array, ndim)

       print *, "ic, Gamma :", ic, res
       
    end do

    print *, "Total norm:", norm

    if(verbose > 0) then
    
    print *, "With Eq. (22): "

    do ic = 0, nc - 1

       res = compute_width_ic_eq22(ic, coeff, ncol, nc, nmult, rc_0, rc_1, lambda_0, lambda_1, ra, Svec, &
            Lvec, Jvec, Delta, dm, ld, mu_array, ndim)

       print *, "ic, Gamma : ", ic, res
       
    end do

    
    open(unit = 10, file ='gamma_test.dat')

    rstep = 0.01d0
    rmax = 10.0d0
    
    call tabulate_gamma_eq22(10, coeff, ncol, nc, nmult, rc_0, rc_1, lambda_0, lambda_1, ra, Svec, &
       Lvec, Jvec, Delta, dm, ld,  mu_array, ndim, rstep, rmax)

    close(10)

 end if
    
    print *, "With Eq. (17): "

    res = compute_width_eq17(coeff, ncol, nc, ld, mu_array, ndim)

    print *, "Gamma : ", res

    open(unit = 10, file = 'contributions_gamma_eq_17_NIJ_1S0_rc_12_nu_1.dat')
    
    call tabulate_gamma_eq17(10, coeff, ncol, nc, ld, mu_array, ndim)

    close(10)
    
  !   norm = compute_H_ev_test(coeff, ncol, nc, 2, rc_0, rc_1, lambda_0, lambda_1, ra, S, &
   !    L, J, Delta, dm, ld, mu_array, ndim, E_c)
 
!     print *, "res: ", norm

    
  
    deallocate(xpoints, P, H, mu_array, xmesh, energies, evec, V, e_phys, ld, qvec, &
         nintervals_array, contributions, Svec, Lvec, Jvec, coeff)

!    call spline_clean()


    
    
  end subroutine solve_cc


  function compute_ld(q, r, ic, l) result(ld)
    !----------------------------------------
    !PURPOSE:
    ! Computes the logarithmic derivative
    !INPUT:
    !q : value of the momentum
    !r : value of r
    !ic : index of the channel, ic = 0, ..., 7
    !l : value of L
    !--------------------------------------------

    complex(DBL), intent(in) :: q
    real(DBL), intent(in) :: r
    integer, intent(in) :: ic, l

    complex(DBL) :: I = cmplx(0.0d0, 1.0d0, DBL), z, h1, h2
    
    complex(DBL) :: ld

    if(l == 0) then

       ld = I*q

    else if(l == 2) then

       z = q*r

       h1 = (1.0d0/z - I)*exp(I*z)

       h2 = (3.0d0/z**2 -3*I/z - 1.0d0)*exp(I*z)

       ld = q*(-2.0d0/z + h1/h2)
       
    else

       ld = 0.0d0

    end if
       
  end function compute_ld
    


  function compute_H_ev(coeff, ncol, nc, nmult, ngauss, rc_0, rc_1, lambda_0, lambda_1, ra, Svec, &
       Lvec, Jvec, Delta, dm, ld, mu_array, ndim, E_c, contributions) &
       result(res)
    !------------------------------------------------------------------
    !PURPOSE:
    ! Computes the expectation value of the Hamiltonian
    !INPUT:
    !coeff: spline coefficients
    !nc:      number of components for the partial wave
    !ncol : 2-> cubic splines, 3-> quintic splines
    !nmult:  number of multipoles
    !rc_0: cut-off radius for T=0
    !rc_1: cut-off radius for T=1
    !lambda_0: strength of annihilation potential for T = 0
    !lambda_1: strength of annihilation potential for T = 1
    !ra : annihilation radius
    !S : spin
    !L : orbital angular momentum
    !Delta: meson-proton mass difference, i.e. m_p-m_m
    !dm : proton-neutron mass difference, m_n-m_p 
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff
    !E_c : Coulomb energy
    !contributions : to the energy
    !--------------------------------------------------------------------------------------------------

    use legendre_new, only : legendre_nodes, legendre_weights
    
    use physical_constants, only : a0

    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, &
         xmesh, nintervals_array => nx_array

    
    integer, intent(in) :: ncol, nc, nmult, ngauss, ndim
    integer, dimension(0:(nmult - 1)), intent(in) :: Svec, Lvec, Jvec
    real(DBL) , intent(in) :: rc_0, rc_1, lambda_0, lambda_1, ra, Delta, dm, E_c
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL), dimension(0:(nc-1)), intent(in) :: mu_array
    complex(DBL), dimension(0:(nc-1), 0:(nc-1)), intent(out) :: contributions
    real(DBL) :: mu, x, r, w
    complex(DBL) :: u, upp, ui, upp_i, uj
    real(DBL), dimension(nc, nc) :: V
    complex(DBL) :: Hu, res, tmp
    integer :: nintervals, i, jp, ix, nx, iinterval, imin, imax, ic, jc, &
         ncoeff_j, ncoeff_i, nsum_i, nsum_j
    real(DBL), dimension(ngauss) :: points, weights
    real(DBL), dimension(:), allocatable :: xpoints, xweights
    real(DBL) :: a, b, k1, k2, mu_1

         
    mu_1 = mu_array(0)

    res = 0.0d0

    u = 0.0d0
    upp = 0.0d0


    call legendre_nodes(points, ngauss)
    call legendre_weights(points, weights, ngauss)

    nintervals = maxval(nintervals_array)
  
    allocate(xpoints(0:(nintervals*ngauss-1)), xweights(0:(nintervals*ngauss-1)))

    nsum_j = 0
    
    do jc = 0, nc - 1
        
       nintervals = nintervals_array(jc)
       ncoeff_j = ncol*(nintervals + 1) - 2
       
       
       nx = 0

    
       do i = 0 , nintervals - 1

          a = xmesh(jc, i)
          b = xmesh(jc, i+1)
          
          k1 = 0.5d0*(b-a)
          k2 = 0.5d0*(b+a)
          
          do ix = 0, ngauss - 1

             nx = nx + 1
             xpoints(nx-1) = k1*points(ix + 1) + k2
             xweights(nx-1) = k1*weights(ix + 1)

             
          end do
       
       end do

       nsum_i = 0
             
       do ic = 0, nc - 1

          contributions(jc, ic) = 0.0d0
          
          ncoeff_i = ncol*(nintervals_array(ic) + 1) - 2
            
          
          do ix = 0, nx - 1
       
             x = xpoints(ix)
             w = xweights(ix)*a0
          
             r = a0*x
       
             iinterval = ix/ngauss + 1
             
             imin = max(0, ncol*(iinterval-1) - 1)
             imax = min(ncoeff_j - 1, ncol*(iinterval+1) - 2)
          
             call pot_isospin(nc, nmult, Svec, Lvec, Jvec, rc_0, rc_1, lambda_0, lambda_1, &
                  ra, r, Delta, dm, E_c, V)

    
             tmp = 0.0d0

             do jp = imin, imax

             
             
                if(jp == ncoeff_j - 1) then

                   
                   
                   uj = Stilde_x(ncol, jc, jp, x, ld(jc))
                
                else 
                
                   uj = S_x(ncol, jc, jp, x) 
                
                end if

      
                
                do i = 0, ncoeff_i - 1
                   
                   if(i == ncoeff_i - 1) then

                      ui = Stilde_x(ncol, ic, i, x, ld(ic))
                      upp_i = Stilde_pp_x(ncol, ic, i, x, ld(ic))
          
                   else 
                
                      ui = S_x(ncol, ic, i, x)
                      upp_i = Spp_x(ncol, ic, i, x)
          
                   end if
                   
                   Hu = mu_1*V(jc+1, ic+1)*ui
                   
                   if(ic == jc) then

                      mu = mu_array(ic)/mu_1
                      mu = mu_1/mu_array(ic)
                      
                      Hu = Hu +  mu*(-upp_i + Lvec(ic/(nc/nmult))*(Lvec(ic/(nc/nmult))+1)/(x*x)*ui)

                   end if
                      
                   res = res +  w*conjg(uj*coeff(nsum_j+jp+1))*Hu*coeff(nsum_i+i+1)

                   contributions(jc, ic) = contributions(jc, ic) +  w*conjg(uj*coeff(nsum_j+jp+1))*Hu*coeff(nsum_i+i+1)
                      
                end do

                
             end do

                
          end do

          
          
          nsum_i = nsum_i + ncoeff_i
      
          
       end do

       nsum_j = nsum_j + ncoeff_j

       
       
    end do


    res = res/mu_1

    

    contributions = contributions/mu_1

    contributions = transpose(contributions)
    
    deallocate(xpoints, xweights)
    
  end function compute_H_ev


!!$  function compute_H_ev_test(coeff, ncol, nc, ngauss, rc_0, rc_1, lambda_0, lambda_1, ra, S, &
!!$       L, J, Delta, dm, ld, mu_array, ndim, E_c) &
!!$       result(res)
!!$    !------------------------------------------------------------------
!!$    !PURPOSE:
!!$    ! Computes the expectation value of the Hamiltonian
!!$    !INPUT:
!!$    !coeff: spline coefficients
!!$    !ncol : 2-> cubic splines, 3-> quintic splines
!!$    !rc_0: cut-off radius for T=0
!!$    !rc_1: cut-off radius for T=1
!!$    !lambda_0: strength of annihilation potential for T = 0
!!$    !lambda_1: strength of annihilation potential for T = 1
!!$    !ra : annihilation radius
!!$    !S : spin
!!$    !L : orbital angular momentum
!!$    !Delta: meson-proton mass difference, i.e. m_p-m_m
!!$    !dm : proton-neutron mass difference, m_n-m_p 
!!$    !ld : logarithmic derivative
!!$    !mu_array : a0**2*m/hbarc**2 for each channel
!!$    !ndim : size of coeff
!!$    !E_c : Coulomb energy
!!$    !contributions : to the energy
!!$    !--------------------------------------------------------------------------------------------------
!!$
!!$    use legendre_new, only : legendre_nodes, legendre_weights
!!$    
!!$    use physical_constants, only : a0
!!$
!!$    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, &
!!$         xmesh, nintervals_array => nx_array
!!$
!!$    
!!$    integer, intent(in) :: ncol, nc, ngauss, S, L, J, ndim
!!$    real(DBL) , intent(in) :: rc_0, rc_1, lambda_0, lambda_1, ra, Delta, dm, E_c
!!$    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
!!$    complex(DBL), dimension(ndim), intent(in) :: coeff
!!$    real(DBL), dimension(0:(nc-1)), intent(in) :: mu_array
!!$  !  complex(DBL), dimension(0:(nc-1), 0:(nc-1)), intent(out) :: contributions
!!$    real(DBL) :: mu, x, r, w
!!$    complex(DBL) :: u, upp, ui, upp_i, uj
!!$    real(DBL), dimension(nc, nc) :: V
!!$    complex(DBL) :: Hu, res, tmp
!!$    integer :: nintervals, i, jp, ix, nx, iinterval, imin, imax, ic, jc, &
!!$         ncoeff_j, ncoeff_i, nsum_i, nsum_j
!!$    real(DBL), dimension(ngauss) :: points, weights
!!$    real(DBL), dimension(:), allocatable :: xpoints, xweights
!!$    real(DBL) :: a, b, k1, k2, mu_1
!!$
!!$    complex(DBL), dimension(:), allocatable :: contributions_r
!!$         
!!$    mu_1 = mu_array(0)
!!$
!!$    res = 0.0d0
!!$
!!$    u = 0.0d0
!!$    upp = 0.0d0
!!$
!!$ !   nintervals = maxval(nintervals_array)
!!$  
!!$ !   allocate(xpoints(0:(nintervals*ngauss-1)), xweights(0:(nintervals*ngauss-1)))
!!$
!!$
!!$    call legendre_nodes(points, ngauss)
!!$    call legendre_weights(points, weights, ngauss)
!!$
!!$
!!$    nsum_j = 0
!!$    
!!$ !   do jc = 0, nc - 1
!!$
!!$    jc = 0
!!$    
!!$    nintervals = nintervals_array(jc)
!!$    ncoeff_j = ncol*(nintervals + 1) - 2
!!$
!!$    allocate(xpoints(0:(nintervals*ngauss-1)), xweights(0:(nintervals*ngauss-1)), &
!!$         contributions_r(0:(nintervals*ngauss-1)))
!!$
!!$       
!!$    nx = 0
!!$
!!$    
!!$    do i = 0 , nintervals - 1
!!$
!!$       a = xmesh(jc, i)
!!$       b = xmesh(jc, i+1)
!!$          
!!$       k1 = 0.5d0*(b-a)
!!$       k2 = 0.5d0*(b+a)
!!$          
!!$       do ix = 0, ngauss - 1
!!$
!!$          nx = nx + 1
!!$          xpoints(nx-1) = k1*points(ix + 1) + k2
!!$          xweights(nx-1) = k1*weights(ix + 1)
!!$             
!!$       end do
!!$       
!!$    end do
!!$
!!$    do ix = 0, nx - 1
!!$
!!$       contributions_r(ix) = 0.0d0
!!$       
!!$       x = xpoints(ix)
!!$       w = xweights(ix)*a0
!!$          
!!$       r = a0*x
!!$       
!!$          
!!$       nsum_i = 0
!!$             
!!$       do ic = 0, nc - 1
!!$          
!!$          ncoeff_i = ncol*(nintervals_array(ic) + 1) - 2
!!$            
!!$          
!!$       
!!$       
!!$          iinterval = ix/ngauss + 1
!!$             
!!$          imin = max(0, ncol*(iinterval-1) - 1)
!!$          imax = min(ncoeff_j - 1, ncol*(iinterval+1) - 2)
!!$          
!!$          call pot_isospin(nc, S, L, J, rc_0, rc_1, lambda_0, lambda_1, &
!!$               ra, r, Delta, dm, E_c, V)
!!$
!!$    
!!$          tmp = 0.0d0
!!$
!!$          do jp = imin, imax
!!$
!!$             
!!$             
!!$             if(jp == ncoeff_j - 1) then
!!$
!!$                uj = Stilde_x(ncol, jc, jp, x, ld(jc))
!!$                
!!$             else 
!!$                
!!$                uj = S_x(ncol, jc, jp, x) 
!!$                
!!$             end if
!!$
!!$      
!!$                
!!$             do i = 0, ncoeff_i - 1
!!$                   
!!$                if(i == ncoeff_i - 1) then
!!$
!!$                   ui = Stilde_x(ncol, ic, i, x, ld(ic))
!!$                   upp_i = Stilde_pp_x(ncol, ic, i, x, ld(ic))
!!$          
!!$                else 
!!$                
!!$                   ui = S_x(ncol, ic, i, x)
!!$                   upp_i = Spp_x(ncol, ic, i, x)
!!$          
!!$                end if
!!$                   
!!$                Hu = mu_1*V(jc+1, ic+1)*ui
!!$                   
!!$                if(ic == jc) then
!!$
!!$                   mu = mu_array(ic)/mu_1
!!$                   mu = mu_1/mu_array(ic)
!!$                   
!!$                   Hu = Hu +  mu*(-upp_i + L*(L+1)/(x*x)*ui)
!!$
!!$                end if
!!$                      
!!$                res = res +  w*conjg(uj*coeff(nsum_j+jp+1))*Hu*coeff(nsum_i+i+1)
!!$                contributions_r(ix) = contributions_r(ix) + w*conjg(uj*coeff(nsum_j+jp+1))*Hu*coeff(nsum_i+i+1)/mu_1
!!$                
!!$  !                 contributions(jc, ic) = contributions(jc, ic) +  w*conjg(uj*coeff(nsum_j+jp+1))*Hu*coeff(nsum_i+i+1)
!!$                      
!!$             end do
!!$
!!$                
!!$          end do
!!$
!!$          nsum_i = nsum_i + ncoeff_i
!!$      
!!$                
!!$       end do
!!$
!!$          
!!$       write(100, *) r, contributions_r(ix)   
!!$       
!!$          
!!$    end do
!!$
!!$       
!!$
!!$       
!!$       
!!$  
!!$
!!$
!!$    res = res/mu_1
!!$
!!$    
!!$
!!$ !   contributions = contributions/mu_1
!!$
!!$ !   contributions = transpose(contributions)
!!$
!!$    
!!$
!!$    
!!$    deallocate(xpoints, xweights, contributions_r)
!!$    
!!$  end function compute_H_ev_test



  
  function compute_norm(coeff, ncol, nc, ngauss, ld, ndim) &
       result(res)
    !------------------------------------------------------------------
    !PURPOSE:
    ! Computes the normalization constant squared.
    !INPUT:
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !ld : logarithmic derivatives
    !ndim : size of coeff
    
    !--------------------------------------------------------------------------------------------------

    use legendre_new, only : legendre_nodes, legendre_weights

    use spline_mod_cubic_quintic, only : S_x, Stilde_x, xmesh, nintervals_array => nx_array

    use physical_constants, only : a0
    
    integer, intent(in) :: ncol, nc, ngauss, ndim
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL) :: x, r, w
    complex(DBL) :: u, ui, uj
    complex(DBL) :: res, tmp
    integer :: nintervals, n_coeff, i, jp, ix, nx, iinterval, imin, imax, jc, ncoeff_sum
    real(DBL), dimension(ngauss) :: points, weights
    real(DBL), dimension(:), allocatable :: xpoints, xweights
    real(DBL) :: a, b, k1, k2
 

    res = 0.0d0

    u = 0.0d0


    call legendre_nodes(points, ngauss)
    call legendre_weights(points, weights, ngauss)

    nintervals = maxval(nintervals_array)
    
    allocate(xpoints(0:(nintervals*ngauss-1)), xweights(0:(nintervals*ngauss-1)) )

    ncoeff_sum = 0

    
    do jc = 0, nc - 1
    
       nintervals = nintervals_array(jc)

       n_coeff = ncol*(nintervals + 1) - 2
       
       
       nx = 0

    
       do i = 0 , nintervals - 1

          a = xmesh(jc, i)
          b = xmesh(jc, i+1)

          k1 = 0.5d0*(b-a)
          k2 = 0.5d0*(b+a)

          do ix = 0, ngauss - 1

             nx = nx + 1
             xpoints(nx-1) = k1*points(ix + 1) + k2
             xweights(nx-1) = k1*weights(ix + 1)

          
          end do
       
       end do

        
       do ix = 0, nx - 1
       
          x = xpoints(ix)
          w = xweights(ix)*a0

          r = a0*x
       
          iinterval = ix/ngauss + 1

          imin = max(0, ncol*(iinterval-1) - 1)
          imax = min(n_coeff - 1, ncol*(iinterval+1) - 2)
          
          tmp = 0.0d0

 
          do jp = 0, n_coeff - 1
             
             
             if(jp == n_coeff - 1) then

                uj = Stilde_x(ncol, jc, jp, x, ld(jc))
                
             else 
                
                uj = S_x(ncol, jc, jp, x) 
                
             end if
       
             do i = 0, n_coeff - 1


                if(i == n_coeff - 1) then

                   ui = Stilde_x(ncol, jc, i, x, ld(jc))
          
                else 
                
                   ui = S_x(ncol, jc, i, x)
          
                end if
                   
                                      
                res = res +  w*conjg(uj*coeff(ncoeff_sum+jp+1))*ui*coeff(ncoeff_sum+i+1)
                   
       
         


             end do
       
          end do

       end do

       ncoeff_sum = ncoeff_sum + n_coeff
       
    end do


    deallocate(xpoints, xweights)
 

  !  res = res

    
  end function compute_norm


  subroutine tabulate_wf(iout, ic, coeff, ncol, nc, ld, ndim, rstep, rmax)

    !------------------------------------------------------------------
    !PURPOSE:
    !Tabulates the wave function for the selected channel
    !INPUT:
    !iout: output file
    !ic: channel
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff
    !rstep : step size
    !rmax : maximum value of r
    !--------------------------------------------------------------------------------------------------


    use physical_constants, only : a0

    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, nintervals_array => nx_array

    
    integer, intent(in) :: iout, ic, ncol, nc, ndim
    real(DBL) , intent(in) :: rstep, rmax
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL) :: x, r
    complex(DBL) :: ui, wf
    integer :: nr, nintervals, ir, i, jc, ncoeff_j, nsum_j

    nr = int(rmax/rstep)



    do ir = 0, nr

       wf = 0.0d0

       r = ir*rstep
       
       x = r/a0

       nsum_j = 0
       
       do jc = 0, ic - 1

          nintervals = nintervals_array(jc)
          ncoeff_j = ncol*(nintervals + 1) - 2
          
          nsum_j = nsum_j + ncoeff_j

       end do
          
             
       nintervals = nintervals_array(ic)
       ncoeff_j = ncol*(nintervals + 1) - 2

       do i = 0, ncoeff_j - 1

          if(i == ncoeff_j - 1) then

             ui = Stilde_x(ncol, ic, i, x, ld(ic))

          else

             ui = S_x(ncol, ic, i, x)

          end if
             
          
          wf = wf +  ui*coeff(nsum_j+i+1)

       
       end do

       write(iout, "(f10.3, 2X, 2(e10.3, 2X))") r, real(wf, DBL), aimag(wf)
       
       
    end do

  end subroutine tabulate_wf


  subroutine tabulate_2nd_deriv_wf(iout, ic, coeff, ncol, nc, ld, ndim, rstep, rmax)

    !------------------------------------------------------------------
    !PURPOSE:
    ! Tabulates the second derivative wave function for all channels
    !INPUT:
    !iout: output file
    !ic: channel
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff
    !rstep : step size
    !rmax : maximum value of r
    !--------------------------------------------------------------------------------------------------


    use physical_constants, only : a0

    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, nintervals_array => nx_array

    
    integer, intent(in) :: iout, ncol, nc, ndim
    real(DBL) , intent(in) :: rstep, rmax
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL) :: x, r
    complex(DBL) :: ui, wf
    integer :: ic, nr, nintervals, ir, i, jc, ncoeff_j, nsum_j

    nr = int(rmax/rstep)



    do ir = 0, nr



       r = ir*rstep + 0.001d0
       
       x = r/a0


       
       wf = 0.0d0

       nsum_j = 0
       
       do jc = 0, ic - 1
          
          nintervals = nintervals_array(jc)
          ncoeff_j = ncol*(nintervals + 1) - 2
          
          nsum_j = nsum_j + ncoeff_j

       end do
       
       
       nintervals = nintervals_array(ic)
       ncoeff_j = ncol*(nintervals + 1) - 2
       
       do i = 0, ncoeff_j - 1
          
          if(i == ncoeff_j - 1) then
             
                ui = Stilde_pp_x(ncol, ic, i, x, ld(ic))
                
             else

                ui = Spp_x(ncol, ic, i, x)
                
             end if
             
             
             wf = wf +  ui*coeff(nsum_j+i+1)

       
       end do

       write(iout, "(f10.3, 2X, 2(e10.3, 2X))") r, real(wf, DBL), aimag(wf)
       
       
    end do

  end subroutine tabulate_2nd_deriv_wf


  subroutine tabulate_wf_2nd_deriv_wf(iout, ic, coeff, ncol, nc, ld, ndim, rstep, rmax)

    !------------------------------------------------------------------
    !PURPOSE:
    ! Tabulates u^* u'' for the selected channel
    !INPUT:
    !iout: output file
    !ic: channel
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !ld : logarithmic derivative
    !ndim : size of coeff
    !rstep : step size
    !rmax : maximum value of r
    !--------------------------------------------------------------------------------------------------


    use physical_constants, only : a0

    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, nintervals_array => nx_array

    
    integer, intent(in) :: iout, ic, ncol, nc, ndim
    real(DBL) , intent(in) :: rstep, rmax
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL) :: x, r
    complex(DBL) :: ui, upp_i, wf, d2wf, product
    integer :: nr, nintervals, ir, i, jc, ncoeff_j, nsum_j

    nr = int(rmax/rstep)



    do ir = 0, nr

       wf = 0.0d0

       d2wf = 0.0d0

       r = ir*rstep + 0.001d0
       
       x = r/a0

       nsum_j = 0
       
       do jc = 0, ic - 1

          nintervals = nintervals_array(jc)
          ncoeff_j = ncol*(nintervals + 1) - 2
          
          nsum_j = nsum_j + ncoeff_j

       end do
          
             
       nintervals = nintervals_array(ic)
       ncoeff_j = ncol*(nintervals + 1) - 2

       do i = 0, ncoeff_j - 1

          if(i == ncoeff_j - 1) then

             ui = Stilde_x(ncol, ic, i, x, ld(ic))
             
             upp_i = Stilde_pp_x(ncol, ic, i, x, ld(ic))

          else

             ui = S_x(ncol, ic, i, x)
             
             upp_i = Spp_x(ncol, ic, i, x)

          end if
             
          
          wf = wf +  ui*coeff(nsum_j+i+1)

          d2wf = d2wf + upp_i*coeff(nsum_j+i+1) 
       
       end do

       product = conjg(wf)*d2wf

       write(iout, "(f10.3, 2X, 2(e10.3, 2X))") r, real(product, DBL), aimag(product)
       
       
    end do

  end subroutine tabulate_wf_2nd_deriv_wf


  subroutine tabulate_wf_2nd_deriv_wf_all(iout, coeff, ncol, nc, ld, ndim, rstep, rmax, mu_array)

    !------------------------------------------------------------------
    !PURPOSE:
    ! Tabulates imaginary part of u^* u'' for the selected channel
    !INPUT:
    !iout: output file
    !ic: channel
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !ld : logarithmic derivative
    !ndim : size of coeff
    !rstep : step size
    !rmax : maximum value of r
    !mu_array : mu/hbarc^2 for each channel
    !--------------------------------------------------------------------------------------------------


    use physical_constants, only : a0

    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, nintervals_array => nx_array

    
    integer, intent(in) :: iout, ncol, nc, ndim
    real(DBL) , intent(in) :: rstep, rmax
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL), dimension(0:(nc-1)), intent(in) :: mu_array
    real(DBL) :: x, r
    complex(DBL) :: ui, upp_i, wf, d2wf
    complex(DBL), dimension(0:(nc-1)) :: res 
    integer :: nr, nintervals, ir, i, ic, jc, ncoeff_j, nsum_j

    nr = int(rmax/rstep)



    do ir = 0, nr

  
       r = ir*rstep + 0.001d0
       
       x = r/a0

       do ic = 0, nc - 1
       
          wf = 0.0d0

          d2wf = 0.0d0

          res(ic) = 0.0d0
       
          nsum_j = 0
       
          do jc = 0, ic - 1

             nintervals = nintervals_array(jc)
             ncoeff_j = ncol*(nintervals + 1) - 2
          
             nsum_j = nsum_j + ncoeff_j

          end do
          
             
          nintervals = nintervals_array(ic)
          ncoeff_j = ncol*(nintervals + 1) - 2

          do i = 0, ncoeff_j - 1

             if(i == ncoeff_j - 1) then
                
                ui = Stilde_x(ncol, ic, i, x, ld(ic))
                
                upp_i = Stilde_pp_x(ncol, ic, i, x, ld(ic))

             else

                ui = S_x(ncol, ic, i, x)
             
                upp_i = Spp_x(ncol, ic, i, x)

             end if
             
          
             wf = wf +  ui*coeff(nsum_j+i+1)

             d2wf = d2wf + upp_i*coeff(nsum_j+i+1) 
       
          end do

          res(ic) = conjg(wf)*d2wf/mu_array(ic)

       end do

       write(iout, "(f10.3, 2X, 5(e10.3, 2X))") r, (aimag(res(ic)), ic = 0, nc - 1), aimag(sum(res))
       
       
    end do

  end subroutine tabulate_wf_2nd_deriv_wf_all

  

  subroutine tabulate_gamma(iout, coeff, ncol, nc, ld, mu_array, ndim, rstep, rmax)
    !------------------------------------------------------------------
    !PURPOSE:
    ! Computes the annihilation distribution \gamma(r)
    !INPUT:
    !iout: output file
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff
    !rstep : step size
    !rmax : maximum value of r
    !--------------------------------------------------------------------------------------------------

    
    use physical_constants, only : a0

    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, nintervals_array => nx_array

    
    integer, intent(in) :: iout, ncol, nc, ndim
    real(DBL) , intent(in) :: rstep, rmax
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL), dimension(0:(nc-1)), intent(in) :: mu_array
    real(DBL) :: x, r, gamma
    complex(DBL) :: ui, upp_j
    integer :: nr, nintervals, ir, i, j, jc, ncoeff_j, nsum_j

    nr = int(rmax/rstep)



    do ir = 0, nr

       gamma = 0.0d0

       r = 0.001d0 + ir*rstep
       
       x = r/a0

       nsum_j = 0
       
       do jc = 0, nc - 1
          
          nintervals = nintervals_array(jc)
          ncoeff_j = ncol*(nintervals + 1) - 2

          do i = 0, ncoeff_j - 1

             if(i == ncoeff_j - 1) then

                ui = Stilde_x(ncol, jc, i, x, ld(jc))

             else

                ui = S_x(ncol, jc, i, x)

             end if
             
             do j = 0, ncoeff_j - 1

                if(j == ncoeff_j - 1) then
            
                   upp_j = Stilde_pp_x(ncol, jc, j, x, ld(jc))
          
                else 
                   upp_j = Spp_x(ncol, jc, j, x)
          
                end if


                
                gamma = gamma +  aimag(conjg(ui*coeff(nsum_j+i+1))*upp_j*coeff(nsum_j+j+1))/mu_array(jc)

       
             end do

          end do          
          
          nsum_j = nsum_j + ncoeff_j
      
          
       end do

       write(iout, "(f5.3, 2X, e10.3)") r, gamma
       
       
    end do

    
  end subroutine tabulate_gamma


  function compute_width_ic(ic, coeff, ncol, nc, nmult, rc_0, rc_1, lambda_0, lambda_1, ra, Svec, &
       Lvec, Jvec, Delta, dm, ld, mu_array, ndim) result(Gamma)
    !------------------------------------------------------------------
    !PURPOSE:
    ! Computes the width by integrating the annihilation distribution
    !\gamma(r) for the given channel
    !INPUT:
    !ic: channel, i.e. ic = 0, ..., nc - 1
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !nc : number of channels
    !rc_0: cut-off radius for T=0
    !rc_1: cut-off radius for T=1
    !lambda_0: strength of annihilation potential for T = 0
    !lambda_1: strength of annihilation potential for T = 1
    !ra : annihilation radius
    !S : spin
    !L : orbital angular momentum
    !Delta: meson-proton mass difference, i.e. m_p-m_m
    !dm : proton-neutron mass difference, m_n-m_p 
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff  
    !--------------------------------------------------------------------------------------------------

    
    use physical_constants, only : a0

    use legendre_new, only : legendre_nodes, legendre_weights
    
    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, nintervals_array => nx_array, xmesh

    
    integer, intent(in) :: ic, ncol, nc, nmult, ndim
    integer, dimension(0:(nmult - 1)), intent(in) :: Svec, Lvec, Jvec
    real(DBL) , intent(in) :: rc_0, rc_1, lambda_0, lambda_1, ra, Delta, dm
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL), dimension(0:(nc-1)), intent(in) :: mu_array
  
    integer :: ngauss, nintervals, ncoeff, nr, i, ir
    real(DBL), dimension(:), allocatable :: points, weights
    real(DBL), dimension(:), allocatable :: rpoints, rweights, gamma_array
    real(DBL) :: a, b, k1, k2, Gamma
    

    
    !Generate the Gauss points and weights for channel ic

    ngauss = 2


    nintervals = nintervals_array(ic)
    
    allocate(points(ngauss), weights(ngauss), rpoints(0:(nintervals*ngauss-1)), rweights(0:(nintervals*ngauss-1)), &
         gamma_array(0:nintervals*ngauss-1))
    
    
    call legendre_nodes(points, ngauss)
    call legendre_weights(points, weights, ngauss)



    ncoeff = ncol*(nintervals + 1) - 2
       
       
    nr = 0

    
    do i = 0 , nintervals - 1

       a = xmesh(ic, i)
       b = xmesh(ic, i+1)

       k1 = 0.5d0*(b-a)*a0
       k2 = 0.5d0*(b+a)*a0

       do ir = 0, ngauss - 1

          nr = nr + 1
          rpoints(nr-1) = k1*points(ir + 1) + k2
          rweights(nr-1) = k1*weights(ir + 1)
          
       end do
       
    end do


    !Compute the distribution

    call compute_gamma_ic(ic, coeff, ncol, nc, nmult,  rc_0, rc_1, lambda_0, lambda_1, ra, Svec, &
       Lvec, Jvec, Delta, dm, ld, mu_array, ndim, rpoints, nr, gamma_array)

    !Integrate

    gamma = 0.0d0
    
    do ir = 0, nr - 1

       gamma = gamma + rweights(ir)*gamma_array(ir)

    end do

    gamma = 2*gamma

    deallocate(points, weights, rpoints, rweights, gamma_array)


    
  end function compute_width_ic

  
  subroutine compute_gamma_ic(ic, coeff, ncol, nc, nmult, rc_0, rc_1, lambda_0, lambda_1, ra, Svec, &
       Lvec, Jvec, Delta, dm, ld, mu_array, ndim, rpoints, nr, gamma_array)
    !------------------------------------------------------------------
    !PURPOSE:
    ! Computes the annihilation distribution \gamma(r) for the given channel
    !INPUT:
    !ic: channel, i.e. ic = 0, ..., nc - 1
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !nc : number of channels
    !rc_0: cut-off radius for T=0
    !rc_1: cut-off radius for T=1
    !lambda_0: strength of annihilation potential for T = 0
    !lambda_1: strength of annihilation potential for T = 1
    !ra : annihilation radius
    !S : spin
    !L : orbital angular momentum
    !Delta: meson-proton mass difference, i.e. m_p-m_m
    !dm : proton-neutron mass difference, m_n-m_p 
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff
    !rpoints: radial points
    !nr: number of points
    !rmax : maximum value of r
    !OUTPUT :
    !gamma_array : computed values \gamma_i(r)
    !--------------------------------------------------------------------------------------------------

    
    use physical_constants, only : a0

    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, nintervals_array => nx_array

    
    integer, intent(in) :: ic, ncol, nc, nmult, ndim, nr
    integer, dimension(0:(nmult - 1)), intent(in) :: Svec, Lvec, Jvec
    real(DBL) , intent(in) :: rc_0, rc_1, lambda_0, lambda_1, ra, Delta, dm
    real(DBL), dimension(0:(nr-1)), intent(in) :: rpoints
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL), dimension(0:(nc-1)), intent(in) :: mu_array
    real(DBL), dimension(0:(nr-1)), intent(out) :: gamma_array
    

    
    real(DBL) :: x, r
    complex(DBL) :: ui, upp_j, Ni, uj
    integer :: ngauss, nintervals, ir, i, jp, jc, ncoeff_i, ncoeff_j, nsum_i, nsum_j, j
    real(DBL), dimension(nc, nc) :: V

    ngauss = 2
    
    Ni = compute_norm_ic(ic, coeff, ncol, nc, ngauss, ld, ndim)

!    write(0, *), "Ni : ", Ni
    
    nsum_i = 0
    
    do jc = 0, ic - 1

       nintervals = nintervals_array(jc)
       ncoeff_j = ncol*(nintervals + 1) - 2
          
       nsum_i = nsum_i + ncoeff_j

    end do
       
    ncoeff_i = ncol*(nintervals_array(ic) + 1) - 2
       
    do ir = 0, nr - 1

       gamma_array(ir) = 0.0d0

       r = rpoints(ir)
       
       x = r/a0

       !diagonal contribution

       do i = 0, ncoeff_i - 1

          if(i == ncoeff_i - 1) then

             ui = Stilde_x(ncol, ic, i, x, ld(ic))

       !      upp_i = Stilde_pp_x(ncol, ic, i, x, ld(ic))
          
          else

             ui = S_x(ncol, ic, i, x)

        !     upp_i = Spp_x(ncol, ic, i, x)
             
          end if


          do jp = 0, ncoeff_i - 1

             if(jp == ncoeff_i - 1) then

                upp_j = Stilde_pp_x(ncol, ic, jp, x, ld(ic))
          
             else

                upp_j = Spp_x(ncol, ic, jp, x)
             
             end if

             
          
             gamma_array(ir) = gamma_array(ir) + aimag(conjg(ui*coeff(nsum_i+i+1))*upp_j*coeff(nsum_i+j+1))/mu_array(ic)

          end do

       end do

       !off-diagonal contribution
              
       call pot_isospin(nc, nmult, Svec, Lvec, Jvec, rc_0, rc_1, lambda_0, lambda_1, &
            ra, r, Delta, dm, 0.0d0, V)


       do jc = 0, nc - 1


          if(ic /= jc) then
             
             nsum_j = 0

             do jp = 0, jc - 1

                nintervals = nintervals_array(jp)
                ncoeff_j = ncol*(nintervals + 1) - 2
          
                nsum_j = nsum_j + ncoeff_j


             end do

             nintervals = nintervals_array(jc)
             
             ncoeff_j = ncol*(nintervals + 1) - 2

             
             do i = 0, ncoeff_i - 1

                if(i == ncoeff_i - 1) then

                   ui = Stilde_x(ncol, ic, i, x, ld(ic))
          
                else

                   ui = S_x(ncol, ic, i, x)
                   
                   
                end if
      
             
                do jp = 0, ncoeff_j - 1

                   if(jp == ncoeff_j - 1) then

                      uj = Stilde_x(ncol, jc, jp, x, ld(jc))
          
                   else
                      
                      uj = S_x(ncol, jc, jp, x)
          
                   end if
                
                   gamma_array(ir) = gamma_array(ir) -  V(ic+1, jc+1)*aimag(conjg(ui*coeff(nsum_i+i+1))*uj*coeff(nsum_j+jp+1))

       
                end do

             end do
          
          end if
       
       end do

    end do

    gamma_array = gamma_array/real(Ni, DBL)
    
    
  end subroutine compute_gamma_ic

  
  function compute_norm_ic(ic, coeff, ncol, nc, ngauss, ld, ndim) &
       result(res)
    !------------------------------------------------------------------
    !PURPOSE:
    ! Computes the normalization constant squared for the given channel.
    !INPUT:
    !ic: index of the channel, i.e. ic = 0, ..., nc - 1
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !ld : logarithmic derivatives
    !ndim : size of coeff
    
    !--------------------------------------------------------------------------------------------------

    use legendre_new, only : legendre_nodes, legendre_weights

    use spline_mod_cubic_quintic, only : S_x, Stilde_x, xmesh, nintervals_array => nx_array

    use physical_constants, only : a0
    
    integer, intent(in) :: ic, ncol, nc, ngauss, ndim
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL) :: x, r, w
    complex(DBL) :: ui, uj
    complex(DBL) :: res, tmp
    integer :: nintervals, ncoeff, i, j, ix, nx, iinterval, imin, imax, jc, nsum
    real(DBL), dimension(ngauss) :: points, weights
    real(DBL), dimension(:), allocatable :: xpoints, xweights
    real(DBL) :: a, b, k1, k2
 

    res = 0.0d0

    nsum = 0
       
    do jc = 0, ic - 1

       nintervals = nintervals_array(jc)
       ncoeff = ncol*(nintervals + 1) - 2
          
       nsum = nsum + ncoeff

    end do
       
    
    call legendre_nodes(points, ngauss)
    call legendre_weights(points, weights, ngauss)

    nintervals = nintervals_array(ic)
    
    allocate(xpoints(0:(nintervals*ngauss-1)), xweights(0:(nintervals*ngauss-1)) )


    ncoeff = ncol*(nintervals + 1) - 2
       
       
    nx = 0

    
    do i = 0 , nintervals - 1

       a = xmesh(ic, i)
       b = xmesh(ic, i+1)

       k1 = 0.5d0*(b-a)
       k2 = 0.5d0*(b+a)

       do ix = 0, ngauss - 1

          nx = nx + 1
          xpoints(nx-1) = k1*points(ix + 1) + k2
          xweights(nx-1) = k1*weights(ix + 1)

          
       end do
       
    end do

        
    do ix = 0, nx - 1
       
       x = xpoints(ix)
       w = xweights(ix)*a0

       r = a0*x
       
       iinterval = ix/ngauss + 1

       imin = max(0, ncol*(iinterval-1) - 1)
       imax = min(ncoeff - 1, ncol*(iinterval+1) - 2)
          
       tmp = 0.0d0

 
       do j = 0, ncoeff - 1
             
             
          if(j == ncoeff - 1) then

             uj = Stilde_x(ncol, ic, j, x, ld(ic))
                
          else 
                
             uj = S_x(ncol, ic, j, x) 
                
          end if
       
          do i = 0, ncoeff - 1


             if(i == ncoeff - 1) then
                
                ui = Stilde_x(ncol, ic, i, x, ld(ic))
          
             else 
                
                ui = S_x(ncol, ic, i, x)
          
             end if
                   
                                      
             res = res +  w*conjg(uj*coeff(nsum+j+1))*ui*coeff(nsum+i+1)
             
       

          end do
       
       end do

    end do

    deallocate(xpoints, xweights)
 

 
    
  end function compute_norm_ic


  function compute_wf(ic, r, coeff, ncol, nc, ld, ndim) result(wf)

    !------------------------------------------------------------------
    !PURPOSE:
    !Computes the wave function u_i(r) for the selected channel
    !INPUT:
    !iout: output file
    !ic: channel
    !r: value of r
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff
    !--------------------------------------------------------------------------------------------------


    use physical_constants, only : a0

    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, nintervals_array => nx_array

    
    integer, intent(in) :: ic, ncol, nc, ndim
    real(DBL), intent(in) :: r 
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL) :: x
    complex(DBL) :: ui, wf
    integer :: nintervals, i, jc, ncoeff_j, nsum_j

    
       
    x = r/a0

    nsum_j = 0
       
    do jc = 0, ic - 1

       nintervals = nintervals_array(jc)
       ncoeff_j = ncol*(nintervals + 1) - 2
          
       nsum_j = nsum_j + ncoeff_j

    end do
          
             
    nintervals = nintervals_array(ic)
    ncoeff_j = ncol*(nintervals + 1) - 2

    wf = 0.0d0
    
    do i = 0, ncoeff_j - 1

       if(i == ncoeff_j - 1) then
          
          ui = Stilde_x(ncol, ic, i, x, ld(ic))
          
       else
          
          ui = S_x(ncol, ic, i, x)
          
       end if
       
          
       wf = wf +  ui*coeff(nsum_j+i+1)

       
    end do


  end function compute_wf


  function compute_contrib_gamma_eq_17(ic, r, coeff, ncol, nc, ld, ndim, mu) result(gamma_i)

    !------------------------------------------------------------------------------------------------
    !PURPOSE:
    !Computes the partial contribution, for the selected channel, to the annihilation density of Eq. (17)
    !INPUT:
    !iout: output file
    !ic: channel
    !r: value of r
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff
    !--------------------------------------------------------------------------------------------------


    use physical_constants, only : a0

    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, nintervals_array => nx_array

    
    integer, intent(in) :: ic, ncol, nc, ndim
    real(DBL), intent(in) :: r, mu 
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL) :: x, gamma_i
    complex(DBL) :: upp_i, ui
    integer :: nintervals, i, ip, jc, ncoeff_j, nsum_j

    
       
    x = r/a0

    nsum_j = 0
       
    do jc = 0, ic - 1

       nintervals = nintervals_array(jc)
       ncoeff_j = ncol*(nintervals + 1) - 2
          
       nsum_j = nsum_j + ncoeff_j

    end do
          
             
    nintervals = nintervals_array(ic)
    ncoeff_j = ncol*(nintervals + 1) - 2

    gamma_i = 0.0d0

    do ip = 0, ncoeff_j - 1

       if(ip == ncoeff_j - 1) then
          
          upp_i = Stilde_pp_x(ncol, ic, ip, x, ld(ic))
          
       else
          
          upp_i = Spp_x(ncol, ic, ip, x)
          
       end if
       
    
       do i = 0, ncoeff_j - 1

          if(i == ncoeff_j - 1) then
          
             ui = Stilde_x(ncol, ic, i, x, ld(ic))
             
          else
             
             ui = S_x(ncol, ic, i, x)
          
          end if
       
          
          gamma_i = gamma_i +  aimag(conjg(ui*coeff(nsum_j+i+1))*upp_i*coeff(nsum_j+ip+1))

       
       end do

    end do

    gamma_i = 2*gamma_i/mu

  end function compute_contrib_gamma_eq_17



  
  function compute_2nd_deriv_wf(ic, r, coeff, ncol, nc, ld, ndim) result(wf)
    !------------------------------------------------------------------
    !PURPOSE:
    ! Tabulates the second derivative wave function for all channels
    !INPUT:
    !iout: output file
    !ic: channel
    !r: value of r
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff
    !--------------------------------------------------------------------------------------------------


    use physical_constants, only : a0

    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, nintervals_array => nx_array

    
    integer, intent(in) :: ic, ncol, nc, ndim
    real(DBL), intent(in) :: r
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL) :: x
    complex(DBL) :: ui, wf
    integer :: nintervals, i, jc, ncoeff_j, nsum_j

    x = r/a0
             
    wf = 0.0d0

    nsum_j = 0
       
    do jc = 0, ic - 1
          
       nintervals = nintervals_array(jc)
       ncoeff_j = ncol*(nintervals + 1) - 2
          
       nsum_j = nsum_j + ncoeff_j

    end do
       
       
    nintervals = nintervals_array(ic)
    ncoeff_j = ncol*(nintervals + 1) - 2
       
    do i = 0, ncoeff_j - 1
          
       if(i == ncoeff_j - 1) then
             
          ui = Stilde_pp_x(ncol, ic, i, x, ld(ic))
                
       else

          ui = Spp_x(ncol, ic, i, x)
                
       end if
             
             
       wf = wf +  ui*coeff(nsum_j+i+1)

       
    end do
       

  end function compute_2nd_deriv_wf


  function compute_width_ic_eq22(ic, coeff, ncol, nc, nmult, rc_0, rc_1, lambda_0, lambda_1, ra, Svec, &
       Lvec, Jvec, Delta, dm, ld, mu_array, ndim) result(Gamma)
    !------------------------------------------------------------------
    !PURPOSE:
    ! Computes the width by integrating the annihilation distribution
    !\gamma(r) for the given channel, defined by Eq. (22)
    !INPUT:
    !ic: channel, i.e. ic = 0, ..., nc - 1
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !nc : number of channels
    !rc_0: cut-off radius for T=0
    !rc_1: cut-off radius for T=1
    !lambda_0: strength of annihilation potential for T = 0
    !lambda_1: strength of annihilation potential for T = 1
    !ra : annihilation radius
    !S : spin
    !L : orbital angular momentum
    !Delta: meson-proton mass difference, i.e. m_p-m_m
    !dm : proton-neutron mass difference, m_n-m_p 
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff  
    !--------------------------------------------------------------------------------------------------

    
    use physical_constants, only : a0

    use legendre_new, only : legendre_nodes, legendre_weights
    
    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, nintervals_array => nx_array, xmesh

    
    integer, intent(in) :: ic, ncol, nc, nmult, ndim
    integer, dimension(0:(nmult - 1)), intent(in) :: Svec, Lvec, Jvec
    real(DBL) , intent(in) :: rc_0, rc_1, lambda_0, lambda_1, ra, Delta, dm
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL), dimension(0:(nc-1)), intent(in) :: mu_array
  
    integer :: ngauss, nintervals, ncoeff, nr, i, ir
    real(DBL), dimension(:), allocatable :: points, weights
    real(DBL), dimension(:), allocatable :: rpoints, rweights, gamma_array
    real(DBL) :: a, b, k1, k2, Gamma
    

    
    !Generate the Gauss points and weights for channel ic

    ngauss = 2


    nintervals = nintervals_array(ic)
    
    allocate(points(ngauss), weights(ngauss), rpoints(0:(nintervals*ngauss-1)), rweights(0:(nintervals*ngauss-1)), &
         gamma_array(0:nintervals*ngauss-1))
    
    
    call legendre_nodes(points, ngauss)
    call legendre_weights(points, weights, ngauss)



    ncoeff = ncol*(nintervals + 1) - 2
       
       
    nr = 0

    
    do i = 0 , nintervals - 1

       a = xmesh(ic, i)
       b = xmesh(ic, i+1)

       k1 = 0.5d0*(b-a)*a0
       k2 = 0.5d0*(b+a)*a0

       do ir = 0, ngauss - 1

          nr = nr + 1
          rpoints(nr-1) = k1*points(ir + 1) + k2
          rweights(nr-1) = k1*weights(ir + 1)
          
       end do
       
    end do


    !Compute the distribution

    call compute_gamma_ic_eq22(ic, coeff, ncol, nc, nmult, rc_0, rc_1, lambda_0, lambda_1, ra, Svec, &
       Lvec, Jvec, Delta, dm, ld, mu_array, ndim, rpoints, nr, gamma_array)

    !Integrate

    gamma = 0.0d0
    
    do ir = 0, nr - 1

       gamma = gamma + rweights(ir)*gamma_array(ir)

    end do

    gamma = 2*gamma

    deallocate(points, weights, rpoints, rweights, gamma_array)


    
  end function compute_width_ic_eq22


  
  subroutine compute_gamma_ic_eq22(ic, coeff, ncol, nc, nmult, rc_0, rc_1, lambda_0, lambda_1, ra, Svec, &
       Lvec, Jvec, Delta, dm, ld, mu_array, ndim, rpoints, nr, gamma_array)
    !-----------------------------------------------------------------------------------------
    !PURPOSE:
    ! Computes the annihilation distribution \gamma(r) for the given channel,
    ! defined by Eq. (22).
    !INPUT:
    !ic: channel, i.e. ic = 0, ..., nc - 1
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !nc : number of channels
    !rc_0: cut-off radius for T=0
    !rc_1: cut-off radius for T=1
    !lambda_0: strength of annihilation potential for T = 0
    !lambda_1: strength of annihilation potential for T = 1
    !ra : annihilation radius
    !S : spin
    !L : orbital angular momentum
    !Delta: meson-proton mass difference, i.e. m_p-m_m
    !dm : proton-neutron mass difference, m_n-m_p 
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff
    !rpoints: radial points
    !nr: number of points
    !rmax : maximum value of r
    !OUTPUT :
    !gamma_array : computed values \gamma_i(r)
    !--------------------------------------------------------------------------------------------------

    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x

    
    integer, intent(in) :: ic, ncol, nc, nmult, ndim, nr
    integer, dimension(0:(nc - 1)), intent(in) :: Svec, Lvec, Jvec
    real(DBL) , intent(in) :: rc_0, rc_1, lambda_0, lambda_1, ra, Delta, dm
    real(DBL), dimension(0:(nr-1)), intent(in) :: rpoints
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL), dimension(0:(nc-1)), intent(in) :: mu_array
    real(DBL), dimension(0:(nr-1)), intent(out) :: gamma_array
    

    
    real(DBL) :: r
    complex(DBL) :: ui, upp_i, Ni, uj
    integer :: ngauss, ir, jc
    real(DBL), dimension(nc, nc) :: V

    ngauss = 2
    
    Ni = compute_norm_ic(ic, coeff, ncol, nc, ngauss, ld, ndim)

    do ir = 0, nr - 1

       gamma_array(ir) = 0.0d0

       r = rpoints(ir)
       
       call pot_isospin(nc, nmult, Svec, Lvec, Jvec, rc_0, rc_1, lambda_0, lambda_1, &
            ra, r, Delta, dm, 0.0d0, V)
       

       ui = compute_wf(ic, r, coeff, ncol, nc, ld, ndim)

       do jc = 0, nc - 1

          uj =  compute_wf(jc, r, coeff, ncol, nc, ld, ndim)

          gamma_array(ir) = gamma_array(ir) - V(ic + 1, jc + 1)*aimag(conjg(ui)*uj)

          if(jc == ic) then
          
             upp_i = compute_2nd_deriv_wf(ic, r, coeff, ncol, nc, ld, ndim)

             gamma_array(ir) = gamma_array(ir) +  aimag(conjg(ui)*upp_i/mu_array(ic))
          
          end if

       end do
          
        
       
    end do

   
    gamma_array = gamma_array/real(Ni, DBL)
    
    
  end subroutine compute_gamma_ic_eq22


  subroutine tabulate_gamma_eq22(iout, coeff, ncol, nc, nmult, rc_0, rc_1, lambda_0, lambda_1, ra, Svec, &
       Lvec, Jvec, Delta, dm, ld,  mu_array, ndim, rstep, rmax)
    !------------------------------------------------------------------------------------------
    !PURPOSE:
    !Tabulate  the annihilation distribution \gamma(r), defined by Eq. (22)
    !INPUT:
    !iout: output file
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !nc : number of channels
    !rc_0: cut-off radius for T=0
    !rc_1: cut-off radius for T=1
    !lambda_0: strength of annihilation potential for T = 0
    !lambda_1: strength of annihilation potential for T = 1
    !ra : annihilation radius
    !S : spin
    !L : orbital angular momentum
    !Delta: meson-proton mass difference, i.e. m_p-m_m
    !dm : proton-neutron mass difference, m_n-m_p 
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff
    !rstep : step size
    !rmax : maximum value of r
    !-------------------------------------------------------------------------------------------------
    
    integer, intent(in) :: iout, ncol, nc, nmult, ndim
    integer, dimension(0:(nmult - 1)), intent(in) :: Svec, Lvec, Jvec
    real(DBL) , intent(in) :: rc_0, rc_1, lambda_0, lambda_1, ra, Delta, dm, rstep, rmax
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL), dimension(0:(nc-1)), intent(in) :: mu_array

    !local variables

    integer :: ir, ic
    real(DBL), dimension(:), allocatable :: rpoints
    real(DBL), dimension(:, :), allocatable :: gamma_array

    integer :: nr

    nr = int(rmax/rstep)

    allocate(rpoints(0:(nr-1)), gamma_array(0:(nc-1), 0:(nr-1)))

    do ir = 0, nr - 1

       rpoints(ir) = 0.001d0 + ir*rstep
       
    end do

    do ic = 0, nc - 1
    
       call compute_gamma_ic_eq22(ic, coeff, ncol, nc, nmult, rc_0, rc_1, lambda_0, lambda_1, ra, Svec, &
            Lvec, Jvec, Delta, dm, ld, mu_array, ndim, rpoints, nr, gamma_array(ic, :))

    end do

!    if(nc == 4) then
    
 !      do ir = 0, nr - 1
    
  !        write(iout, "(f5.3, 4(2X, e10.3))") rpoints(ir), gamma_array(0:(nc-1), ir)
       
  !     end do

  !  end if

    deallocate(rpoints, gamma_array)
    
  end subroutine tabulate_gamma_eq22

  subroutine compute_gamma_ic_eq17(ic, coeff, ncol, nc, ld, mu_array, ndim, rpoints, nr, gamma_array)
    !-----------------------------------------------------------------------------------------
    !PURPOSE:
    ! Computes the contribution from the given channel to the annihilation distribution \gamma(r),
    ! defined by Eq. (17).
    !INPUT:
    !ic : index of the channel
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !nc : number of channels
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff
    !rpoints: radial points
    !nr: number of points for each channel
    !rmax : maximum value of r
    !OUTPUT :
    !gamma_array : computed values \gamma_i(r)
    !--------------------------------------------------------------------------------------------------

    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x

    
    integer, intent(in) :: ic, ncol, nc, ndim, nr
    real(DBL), dimension(0:(nr-1)), intent(in) :: rpoints
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL), dimension(0:(nc-1)), intent(in) :: mu_array
    real(DBL), dimension(0:(nr-1)), intent(out) :: gamma_array
    

    
    real(DBL) :: r
    complex(DBL) :: ui, upp_i, Ni
    integer :: ngauss, ir, jc

    ngauss = 2

    Ni = 0.0d0

    do jc = 0, nc - 1

       Ni = Ni + compute_norm_ic(jc, coeff, ncol, nc, ngauss, ld, ndim)

    end do
       
 !   Ni = compute_norm_ic(ic, coeff, ncol, nc, ngauss, ld, ndim)

    do ir = 0, nr - 1
            
       gamma_array(ir) = 0.0d0

       r = rpoints(ir)

       
       ui = compute_wf(ic, r, coeff, ncol, nc, ld, ndim)

       upp_i = compute_2nd_deriv_wf(ic, r, coeff, ncol, nc, ld, ndim)

       gamma_array(ir) = aimag(conjg(ui)*upp_i/mu_array(ic))
             
    end do
    
    gamma_array = gamma_array/real(Ni, DBL)
    
    
  end subroutine compute_gamma_ic_eq17


  subroutine tabulate_gamma_eq17(iout, coeff, ncol, nc, ld, mu_array, ndim)
    !------------------------------------------------------------------
    !PURPOSE:
    !Tabulates the contributions to the annihilation distribution
    !\gamma(r, defined by Eq. (22)
    !INPUT:
    !iout: output file
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !nc : number of channels
    !rc_0: cut-off radius for T=0
    !rc_1: cut-off radius for T=1
    !lambda_0: strength of annihilation potential for T = 0
    !lambda_1: strength of annihilation potential for T = 1
    !ra : annihilation radius
    !S : spin
    !L : orbital angular momentum
    !Delta: meson-proton mass difference, i.e. m_p-m_m
    !dm : proton-neutron mass difference, m_n-m_p 
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff  
    !--------------------------------------------------------------------------------------------------

    use physical_constants, only : a0

    use legendre_new, only : legendre_nodes, legendre_weights

    use spline_mod_cubic_quintic, only : nintervals_array => nx_array, xmesh

    
    integer, intent(in) :: iout, ncol, nc, ndim
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL), dimension(0:(nc-1)), intent(in) :: mu_array
    
    integer :: ngauss, nintervals, ic, i, ir, nr
    real(DBL), dimension(:), allocatable :: points, weights
    real(DBL), dimension(:), allocatable :: rpoints, rweights 
    real(DBL), dimension(:, :), allocatable :: gamma_array
    real(DBL) :: a, b, k1, k2, s
 

    
    ngauss = 2
    
    ic = 0

    nintervals = nintervals_array(ic)

    allocate(points(ngauss), weights(ngauss), rpoints(0:(nintervals*ngauss-1)), &
         rweights(0:(nintervals*ngauss-1)), gamma_array(0:(nc-1), 0:nintervals*ngauss-1))
   
    call legendre_nodes(points, ngauss)
    call legendre_weights(points, weights, ngauss)

    
    nr = 0
    
    do i = 0 , nintervals - 1
       
       a = xmesh(ic, i)
       b = xmesh(ic, i+1)
          
       k1 = 0.5d0*(b-a)*a0
       k2 = 0.5d0*(b+a)*a0

       do ir = 0, ngauss - 1

          nr = nr + 1
          rpoints(nr - 1) = k1*points(ir + 1) + k2
          rweights(nr -1 ) = k1*weights(ir + 1)
          
       end do
       
    end do

    do ic = 0, nc - 1

       do ir = 0, nr -1
       
          gamma_array(ic, ir) = compute_contrib_gamma_eq_17(ic, rpoints(ir), coeff, ncol, nc, ld, ndim, mu_array(ic))
       
    !   call compute_gamma_ic_eq17(ic, coeff, ncol, nc, ld, mu_array, ndim, rpoints, nr, gamma_array(ic, :))

       end do

    end do

    if(nc == 4) then

       s = 0.0d0
       
       do ir = 0, nr - 1

          write(iout, "(e10.3, 4(2X, e10.3), 2X, e10.3)") rpoints(ir), (gamma_array(ic, ir), ic = 0, nc - 1), &
                gamma_array(1, ir) + gamma_array(3, ir) 

          s = s + rweights(ir)*(gamma_array(1, ir) + gamma_array(3, ir))
          
       end do

       print *, "s: ", s

       print *, "r(0): ", rpoints(0)
       
    end if

    if(nc == 8) then

       do ir = 0, nr - 1

          write(iout, "(e10.3, 8(2X, e10.3), 2X, e10.3, 2X, e10.3, 2X, e10.3)") rpoints(ir), &
               (gamma_array(ic, ir), ic = 0, nc - 1), &
               gamma_array(1, ir) + gamma_array(3, ir), gamma_array(5, ir) + gamma_array(7, ir), &
               gamma_array(1, ir) + gamma_array(3, ir) + gamma_array(5, ir) + gamma_array(7, ir)

       end do

    end if

    

    deallocate(points, weights, rpoints, rweights, gamma_array)
  
    
  end subroutine tabulate_gamma_eq17
 
    
  
  function compute_width_eq17(coeff, ncol, nc, ld, mu_array, ndim) result(Gamma)
    !------------------------------------------------------------------
    !PURPOSE:
    ! Computes the width by integrating the annihilation distribution
    !\gamma(r) for the given channel, defined by Eq. (22)
    !INPUT:
    !ic: channel, i.e. ic = 0, ..., nc - 1
    !coeff: spline coefficients
    !ncol : 2-> cubic splines, 3-> quintic splines
    !nc : number of channels
    !rc_0: cut-off radius for T=0
    !rc_1: cut-off radius for T=1
    !lambda_0: strength of annihilation potential for T = 0
    !lambda_1: strength of annihilation potential for T = 1
    !ra : annihilation radius
    !S : spin
    !L : orbital angular momentum
    !Delta: meson-proton mass difference, i.e. m_p-m_m
    !dm : proton-neutron mass difference, m_n-m_p 
    !ld : logarithmic derivative
    !mu_array : a0**2*m/hbarc**2 for each channel
    !ndim : size of coeff  
    !--------------------------------------------------------------------------------------------------

    
    use physical_constants, only : a0

    use legendre_new, only : legendre_nodes, legendre_weights
    
    use spline_mod_cubic_quintic, only : Spp_x, Stilde_pp_x, S_x, Stilde_x, nintervals_array => nx_array, xmesh

    
    integer, intent(in) :: ncol, nc, ndim
    complex(DBL), dimension(0:(nc-1)), intent(in) :: ld
    complex(DBL), dimension(ndim), intent(in) :: coeff
    real(DBL), dimension(0:(nc-1)), intent(in) :: mu_array
  
    integer :: ngauss, nintervals, ic, ncoeff, i, ir
    integer :: nr
    real(DBL), dimension(:), allocatable :: points, weights
    real(DBL), dimension(:, :), allocatable :: rpoints, rweights, gamma_array
    real(DBL) :: a, b, k1, k2, Gamma, Gamma_i
    

    
    !Generate the Gauss points and weights for channel ic

    ngauss = 2

    nintervals = maxval(nintervals_array)

    allocate(points(ngauss), weights(ngauss), rpoints(0:(nc-1), 0:(nintervals*ngauss-1)), &
         rweights(0:(nc-1), 0:(nintervals*ngauss-1)), gamma_array(0:(nc-1), 0:nintervals*ngauss-1))
   
    call legendre_nodes(points, ngauss)
    call legendre_weights(points, weights, ngauss)

    gamma = 0.0d0

    print *, "Partial widths:"
    
    do ic = 0, nc - 1

       Gamma_i = 0.0d0
       
       nintervals = nintervals_array(ic)

       ncoeff = ncol*(nintervals + 1) - 2
       
       nr = 0
    
       do i = 0 , nintervals - 1

          a = xmesh(ic, i)
          b = xmesh(ic, i+1)
          
          k1 = 0.5d0*(b-a)*a0
          k2 = 0.5d0*(b+a)*a0

          do ir = 0, ngauss - 1

             nr = nr + 1
             rpoints(ic, nr - 1) = k1*points(ir + 1) + k2
             rweights(ic, nr -1 ) = k1*weights(ir + 1)
          
          end do
       
       end do

       call compute_gamma_ic_eq17(ic, coeff, ncol, nc, ld, mu_array, ndim, &
            rpoints(ic, 0:(nr-1)), nr, gamma_array(ic, 0:(nr-1)))

       do ir = 0, nr - 1

          gamma = gamma + 2*rweights(ic, ir)*gamma_array(ic, ir)

          Gamma_i = Gamma_i + 2*rweights(ic, ir)*gamma_array(ic, ir)
          
       end do

       print *, "i, Gamma_ic: ", ic, Gamma_i
       
       
       do ir = 0, nr - 1
       
          write(11 + ic, "(f5.3, (2X, e10.3))") rpoints(ic, ir), gamma_array(ic, ir) 

       end do
       
    end do

      
    deallocate(points, weights, rpoints, rweights, gamma_array)


    
  end function compute_width_eq17



  
  subroutine pot_isospin(nc, nmult, Svec, Lvec, Jvec, rc_0, rc_1, lambda_0, lambda_1, &
       ra, r, Delta, dm, E_c, V)
    !---------------------------------------------------------------------
    !PURPOSE:
    !         Computes the regularized potential for all radial points
    !         in proton-neutron basis
    !INPUT:
    !nc:      number of components for the partial wave
    !nmult:  number of multipoles
    !S: spin of each multipole
    !L: orbital angular momentum of each multipole
    !J: total angular momentum of each multipole
    !rc_0: cut-off radius for T=0
    !rc_1: cut-off radius for T=1
    !lambda_0: strength of annihilation potential for T = 0
    !lambda_1: strength of annihilation potential for T = 1
    !ra: annihilation radius
    !r: radial mesh point
    !Delta: meson-proton mass difference, i.e. m_p-m_m
    !dm : proton-neutron mass difference, m_n-m_p
    !E_c : Coulomb energy
    !OUTPUT:
    !------------------------------------------------------------------------------
    !V: Potential
    !------------------------------------------------------------------------------

    use potential_regularization, only: MCUT, NCUT
    use physical_constants, only: alpha, hbarc
    
    integer, intent(in) :: nc, nmult
    integer, dimension(0:(nmult - 1)), intent(in) :: Svec, Lvec, Jvec
    real(DBL), intent(in) :: ra, lambda_0, lambda_1, rc_0, rc_1,  r, Delta, dm, E_c
    real(DBL), dimension(nc, nc), intent(out) :: V
    
    !local variables

    integer :: T, ic
    real(DBL) :: V_0, V_1,  x, Va_0, Va_1, V_c, Ec_MeV
    
    
    
    V = 0.0d0

    T = 0

    if(VMODEL == 0) then
    
       call V_NN_BP_reg(T, Svec(0), Jvec(0), Lvec(0), Lvec(0), NCUT, MCUT, rc_0, r, V_0)

    else if(VMODEL == 1) then

       call V_NN_NIJ_reg(T, Svec(0), Jvec(0), Lvec(0), Lvec(0), NCUT, MCUT, rc_0, r, V_0)

    else

       return

    end if
       
  !  V_0 = 0.0d0
    
    T = 1

    if(VMODEL == 0) then
    
       call V_NN_BP_reg(T, Svec(0), Jvec(0), Lvec(0), Lvec(0), NCUT, MCUT, rc_1, r, V_1)    

    else if(VMODEL == 1) then

       call V_NN_NIJ_reg(T, Svec(0), Jvec(0), Lvec(0), Lvec(0), NCUT, MCUT, rc_1, r, V_1)    

    else

       return

    end if
       
       
  !  V_1 = 0.0d0
    
    x = r/ra

    Va_0 = -lambda_0*hbarc*exp(-x)/r

    Va_1 = -lambda_1*hbarc*exp(-x)/r

    V_c = -hbarc*alpha/r

    Ec_Mev = E_c*1.0d-3
    
!    V_c = 0.0d0

!    V_1 = V_0
    
    if(nc == 1) then

       V(1, 1) = 0.5d0*(V_0 + V_1) + V_c  - Ec_MeV

    end if

    if(nc == 2) then

       V(1, 1) = 0.5d0*(V_0 + V_1) + V_c - Ec_MeV  

       V(2, 1) = 0.5d0*(Va_0 + Va_1)

       V(1, 2) = V(2, 1)

       V(2, 2) = -2*Delta - Ec_MeV

    end if
          
    
    if(nc == 4 .or. nc == 8) then       
       
       V(1, 1) = 0.5d0*(V_0 + V_1) -hbarc*alpha/r - Ec_MeV  

       V(2, 1) = 0.5d0*(Va_0 + Va_1)

       V(3, 1) = 0.5d0*(V_0 - V_1)

       V(4, 1) = 0.5d0*(Va_0 - Va_1)

       V(1, 2) = V(2, 1)

       V(2, 2) = -2*Delta - Ec_MeV

       V(3, 2) = 0.5d0*(Va_0 - Va_1)

       V(4, 2) = 0.0d0

       V(1, 3) = V(3, 1)

       V(2, 3) = V(3, 2)

       V(3, 3) = 0.5d0*(V_0 + V_1) + 2*dm - Ec_MeV

       V(4, 3) = 0.5d0*(Va_0 + Va_1)

       V(1, 4) = V(4, 1)

       V(2, 4) = V(4, 2)

       V(3, 4) = V(4, 3)

       V(4, 4) = -2*Delta  - Ec_MeV
   
    end if

    if(nc == 8) then

       T = 0
    
       if(VMODEL == 0) then
  
          call V_NN_BP_reg(T, Svec(1), Jvec(1), Lvec(1), Lvec(1), NCUT, MCUT, rc_0, r, V_0)

       else if(VMODEL == 1) then

          call V_NN_NIJ_reg(T, Svec(1), Jvec(1), Lvec(1), Lvec(1), NCUT, MCUT, rc_0, r, V_0)

       else

          return

       end if
          
  !  V_0 = 0.0d0
    
       T = 1

       if(VMODEL == 0) then
  
          call V_NN_BP_reg(T, Svec(1), Jvec(1), Lvec(1), Lvec(1), NCUT, MCUT, rc_1, r, V_1)     

       else if(VMODEL == 1) then

          call V_NN_NIJ_reg(T, Svec(1), Jvec(1), Lvec(1), Lvec(1), NCUT, MCUT, rc_1, r, V_1)     


       else 

          return

       end if
          
       V(nc + 1, nc + 1) = 0.5d0*(V_0 + V_1) -hbarc*alpha/r - Ec_MeV  

       V(nc + 2, nc + 1) = 0.5d0*(Va_0 + Va_1)

       V(nc + 3, nc + 1) = 0.5d0*(V_0 - V_1)

       V(nc + 4, nc + 1) = 0.5d0*(Va_0 - Va_1)

       V(nc + 1, nc + 2) = V(nc + 2, nc + 1)

       V(nc + 2, nc + 2) = -2*Delta - Ec_MeV

       V(nc + 3, nc + 2) = 0.5d0*(Va_0 - Va_1)

       V(nc + 4, nc + 2) = 0.0d0

       V(nc + 1, nc + 3) = V(nc + 3, nc + 1)

       V(nc + 2, nc + 3) = V(nc + 3, nc + 2)

       V(nc + 3, nc + 3) = 0.5d0*(V_0 + V_1) + 2*dm - Ec_MeV

       V(nc + 4, nc + 3) = 0.5d0*(Va_0 + Va_1)

       V(nc + 1, nc + 4) = V(nc + 4, nc + 1)

       V(nc + 2, nc + 4) = V(nc + 4, nc + 2)

       V(nc + 3, nc + 4) = V(nc + 4, nc + 3)

       V(nc + 4, nc + 4) = -2*Delta  - Ec_MeV
   
    
       T = 0

       if(VMODEL == 0) then
       
          call V_NN_BP_reg(T, Svec(0), Jvec(0), Lvec(0), Lvec(1), NCUT, MCUT, rc_0, r, V_0)

       else if(VMODEL == 1) then

          call V_NN_NIJ_reg(T, Svec(0), Jvec(0), Lvec(0), Lvec(1), NCUT, MCUT, rc_0, r, V_0)

       else

          return

       end if
          
       !  V_0 = 0.0d0
    
       T = 1

       if(VMODEL == 0) then
       
          call V_NN_BP_reg(T, Svec(0), Jvec(0), Lvec(0), Lvec(1), NCUT, MCUT, rc_1, r, V_1)     

       else if(VMODEL == 1) then

          call V_NN_NIJ_reg(T, Svec(0), Jvec(0), Lvec(0), Lvec(1), NCUT, MCUT, rc_1, r, V_1)     

       else

          return

       end if

       

       V(1, nc + 1) = 0.5d0*(V_0 + V_1)  

       V(2, nc + 1) = 0.0d0

       V(3, nc + 1) = 0.5d0*(V_0 - V_1)

       V(4, nc + 1) = 0.0d0

       V(1, nc + 2) = V(2, nc + 1)

       V(2, nc + 2) = 0.0d0

       V(3, nc + 2) = 0.0d0

       V(4, nc + 2) = 0.0d0

       V(1, nc + 3) = V(3, nc + 1)

       V(2, nc + 3) = V(3, nc + 2)

       V(3, nc + 3) = 0.5d0*(V_0 + V_1)

       V(4, nc + 3) = 0.0d0

       V(1, nc + 4) = V(4, nc + 1)

       V(2, nc + 4) = V(4, nc + 2)

       V(3, nc + 4) = V(4, nc + 3)

       V(4, nc + 4) = 0.0d0
   

       do ic = 1, nc

          V(nc + ic, ic) = V(ic, nc + ic)

       end do

    end if
       
  end subroutine pot_isospin


  subroutine V_NN_BP_reg(T, S, J, L, LP, n, MCUT, r_c, r, V)
   !-----------------------------------------------------------
   !PURPOSE:
   !        Implementation of the N-NB B-P potential with
   !        regularization
   !INPUT:
   !T: Isopin (0 or 1)
   !S: spin
   !L: angular momentum of final state
   !LP: angular momentum of initial state
   !n: power of the polynomial
   !MCUT: kind of regularization
   !     0 -> V=0 for r <= r_c
   !     1 -> V=const for r < = r_c
   !     2 -> V linear for r< = r_c and V(0) = 0
   !     3 -> V=\beta*x^n + \gamma*x^{n+1} for r <= r_c
   !     4 -> V=\beta*x^n+\gamma*x^{n+1} +\delta*x^{n+2}
   !r_c: cut-off
   !r: distance from origin
   !OUTPUT:
   !V: value of potential at r
   !-------------------------------------------------------------

   integer, intent(in) :: T, S, J, L, LP, n, MCUT
   real(DBL), intent(in) :: r_c, r
   real(DBL), intent(out) :: V
 
   !constants
   real(DBL), parameter :: eps = 1.0d-4

   !local variables
   real(DBL) :: x, V_c, V_c_p, V_c_m, V_pc, beta, gamma, delta, V_ppc

   x = r/r_c

   V = 0.0d0
   
   if(x > 1.0d0) then

      call V_NN_BP(T, S, J, L, LP, r, V)

   else

      call V_NN_BP(T, S, J, L, LP, r_c, V_c)   !potential at r=r_c

      if(MCUT == 0) then

         V = 0.0d0

      else if(MCUT == 1) then

         V = V_c

      else if(MCUT == 2) then

         V = x*V_c

      else if(MCUT == 3) then

         call V_NN_BP(T, S, J, L, LP, r_c + eps, V_c_p)

         call V_NN_BP(T, S, J, L, LP, r_c - eps, V_c_m)

         V_pc = (V_c_p - V_c_m)/(2*eps)  !V'(r_c)

         beta = (n+1.0d0)*V_c - V_pc*r_c

         gamma = V_pc*r_c -n*v_c

         V = (beta+gamma*x)*x**n

      else if(MCUT == 4) then

         call V_NN_BP(T, S, J, L, LP, r_c + eps, V_c_p)
         
         call V_NN_BP(T, S, J, L, LP, r_c - eps, V_c_m)

         V_pc = r_c*(V_c_p - V_c_m)/(2*eps)  !V'(x=1)
         V_ppc = r_c*r_c*(V_c_m + V_c_p - 2.0d0*V_c)/(eps*eps)

         beta = 0.5d0*(V_ppc - 2*(n+1)*V_pc + (n+1)*(n+2)*V_c)
         gamma = -V_ppc + (2*n+1)*V_pc - n*(n+2)*V_c
         delta = 0.5d0*(V_ppc - 2*n*V_pc + n*(n+1)*V_c)

         V = (beta + gamma*x + delta*x*x)*x**N

      end if

   end if

 end subroutine V_NN_BP_reg


 subroutine V_NN_NIJ_reg(T, S, J, L, LP, n, MCUT, r_c, r, V)
   !-----------------------------------------------------------
   !PURPOSE:
   !        Implementation of the N-NB Nijmegen potential with
   !        regularization
   !INPUT:
   !T: Isopin (0 or 1)
   !S: spin
   !L: angular momentum of final state
   !LP: angular momentum of initial state
   !n: power of the polynomial
   !MCUT: kind of regularization
   !     0 -> V=0 for r <= r_c
   !     1 -> V=const for r < = r_c
   !     2 -> V linear for r< = r_c and V(0) = 0
   !     3 -> V=\beta*x^n + \gamma*x^{n+1} for r <= r_c
   !     4 -> V=\beta*x^n+\gamma*x^{n+1} +\delta*x^{n+2}
   !r_c: cut-off
   !r: distance from origin
   !OUTPUT:
   !V: value of potential at r
   !-------------------------------------------------------------

   integer, intent(in) :: T, S, J, L, LP, n, MCUT
   real(DBL), intent(in) :: r_c, r
   real(DBL), intent(out) :: V
 
   !constants
   real(DBL), parameter :: eps = 1.0d-4

   !local variables
   real(DBL) :: x, V_c, V_c_p, V_c_m, V_pc, beta, gamma, delta, V_ppc

   x = r/r_c

   V = 0.0d0
   
   if(x > 1.0d0) then

      call V_NN_NIJ(T, S, J, L, LP, r, V)

   else

      call V_NN_NIJ(T, S, J, L, LP, r_c, V_c)   !potential at r=r_c

      if(MCUT == 0) then

         V = 0.0d0

      else if(MCUT == 1) then

         V = V_c

      else if(MCUT == 2) then

         V = x*V_c

      else if(MCUT == 3) then

         call V_NN_NIJ(T, S, J, L, LP, r_c + eps, V_c_p)

         call V_NN_NIJ(T, S, J, L, LP, r_c - eps, V_c_m)

         V_pc = (V_c_p - V_c_m)/(2*eps)  !V'(r_c)

         beta = (n+1.0d0)*V_c - V_pc*r_c

         gamma = V_pc*r_c -n*v_c

         V = (beta+gamma*x)*x**n

      else if(MCUT == 4) then

         call V_NN_NIJ(T, S, J, L, LP, r_c + eps, V_c_p)
         
         call V_NN_NIJ(T, S, J, L, LP, r_c - eps, V_c_m)

         V_pc = r_c*(V_c_p - V_c_m)/(2*eps)  !V'(x=1)
         V_ppc = r_c*r_c*(V_c_m + V_c_p - 2.0d0*V_c)/(eps*eps)

         beta = 0.5d0*(V_ppc - 2*(n+1)*V_pc + (n+1)*(n+2)*V_c)
         gamma = -V_ppc + (2*n+1)*V_pc - n*(n+2)*V_c
         delta = 0.5d0*(V_ppc - 2*n*V_pc + n*(n+1)*V_c)

         V = (beta + gamma*x + delta*x*x)*x**N

      end if

   end if

 end subroutine V_NN_NIJ_reg

 
      
 subroutine V_NN_BP(T, S, J, L, LP, r, V)
   !------------------------------------------------------------
   !PURPOSE:
   !        Implementation of the N-NB Bryan-Phillips potential
   !        R. A. Bryan and R. J. N Phillips
   !        NPB 5 (1968) 201
   !INPUT:
   !T: Isopin (0 or 1)
   !S: spin
   !L: angular momentum of final state
   !LP: angular momentum of initial state
   !r: distance from origin
   !OUTPUT:
   !V: value of potential at r
   !-------------------------------------------------------------

   use physical_constants, only: hbarc, M_N

   integer, intent(in) :: T, S, J, L, LP
   real(DBL), intent(in) :: r
   real(DBL), intent(out) :: V

   !Constants

   !Pseudo-scalars (\pi, \eta)
   integer, parameter :: n_ps = 2 !Number of pseudo scalars
   integer, parameter, dimension(n_ps) :: iso_ps = (/ 1, 0 /)  !iso-spin
   real(DBL), dimension(n_ps), parameter :: m_ps = (/ 138.2d0, 548.0d0 /)  !masses
   real(DBL), dimension(n_ps), parameter :: g2_ps = (/ 13.5d0, 7.0d0 /) !couplings

   
   !Vectors (\rho, \omega)

   integer, parameter :: n_v = 2  !Number of vectors (\rho, \omega)
   integer, parameter, dimension(n_v) :: iso_v = (/ 1, 0 /)  !iso-spin
   real(DBL), dimension(n_v), parameter :: m_v = (/ 760.0d0, 782.0d0 /)  !masses
   real(DBL), dimension(n_v), parameter :: g2_v = (/ 0.68d0, 21.5d0 /) !couplings
   real(DBL), dimension(n_v), parameter :: fg_v = (/ 4.4d0, 0.0d0 /) !f/g

   !Scalars (\sigma_1, \sigma_2)
   integer, parameter :: n_s = 2 !Number of  scalars
   integer, parameter, dimension(n_s) :: iso_s = (/ 0, 1 /)  !iso-spin
   real(DBL), dimension(n_ps), parameter :: m_s = (/ 560.0d0, 770.0d0 /)  !masses
   real(DBL), dimension(n_ps), parameter :: g2_s = (/ 9.4d0, 6.1d0 /) !couplings

      
   !local variables

   integer :: t1_t2, s1_s2, f_iso, t_c, i
   real(DBL) :: s12_me, so_me, y, x, b, r1, r2, r3
   real(DBL), dimension(n_ps) :: V_ps
   real(DBL), dimension(n_v) :: V_v
   real(DBL), dimension(n_s) :: V_s
   
   V = 0.0d0

   t1_t2 = 2*T*(T+1) - 3      !matrix element of \tau_1*\tau_2
   s1_s2 = 2*S*(S+1) - 3      !matrix element of \sigma_1*\sigma_2
   s12_me = S_12(S, J, L, LP)       !matrix element of S_12
   so_me = spin_orbit(S, J, L, LP)  !matrix element of L*S
   t_c = 1
   
   
   if(L /= LP) then

      t_c = 0
      s1_s2 = 0

   end if

   
   !iso-scalar exchanges

   do i = 1, n_ps
   
      f_iso = 1 + iso_ps(i)*(-1 + t1_t2)
      y = (m_ps(i)/(2*M_N))**2
      x = m_ps(i)*r/hbarc
      V = y*(s1_s2/3.0d0 + F_T(x)*s12_me)
      V_ps(i) = f_iso*g2_ps(i)*m_ps(i)*V*yukawa(x)

   end do


   !vector exchanges

   do i = 1, n_v

      f_iso = 1 + iso_v(i)*(-1 + t1_t2)
      b = fg_v(i)
      y = (m_v(i)/(2*M_N))**2
      x = m_v(i)*r/hbarc
      r1 = (1.0d0 + y*(1.0d0+b))**2      !Eq. (9)
      r2 = y*((1.0d0+b)**2)/3.0d0              !Eq. (10)
      r3 = 2*y*(3.0d0+4.0d0*b+3.0d0*y*b*b) !(12)
      V = r1*t_c + r2*(2.0d0*s1_s2-3.0d0*F_T(x)*s12_me) - r3*F_LS(x)*so_me
      V_v(i) = f_iso*g2_v(i)*m_v(i)*V*yukawa(x)

   end do
      

   !Scalar exchanges

   do i = 1, n_s

      f_iso = 1 + iso_s(i)*(-1 + t1_t2)
      y = (m_s(i)/(2*M_N))**2
      x = m_s(i)*r/hbarc
      V = (y-1.0d0)*(t_c+2*y*F_LS(x)*so_me)
      V_s(i) = f_iso*g2_s(i)*m_s(i)*V*yukawa(x)

   end do
  

   V = - V_ps(1) + V_ps(2) + V_v(1) - V_v(2) + V_s(1) - V_s(2) 
   
   
 end subroutine V_NN_BP


 subroutine V_NN_NIJ(T, S, J, L, LP, r, V)
   !------------------------------------------------------------
   !PURPOSE:
   !        Implementation of the Nijmegen potential
   !        P. H. Timmers, W. A. van der Sanden, J. J de Swart,
   !        Phys. Rev. D 29 (1984) 1928 (16). and
   !        M. M. Nagels, T. A. Rijken, J. J. de Swart,
   !        Phys. Rev. D 12 (1975) 744.
   !INPUT:
   !T: Isopin (0 or 1)
   !S: spin
   !L: angular momentum of final state
   !LP: angular momentum of initial state
   !r: distance from origin
   !OUTPUT:
   !V: value of potential at r
   !-------------------------------------------------------------

   use physical_constants, only: hbarc, M_N

   integer, intent(in) :: T, S, J, L, LP
   real(DBL), intent(in) :: r
   real(DBL), intent(out) :: V

   !Constants

   !Pseudo-scalars (\pi, \eta, X^0)
   integer, parameter :: n_ps = 3 !Number of pseudo scalars
   integer, parameter, dimension(n_ps) :: iso_ps = (/ 1, 0, 0 /)  !iso-spin
   real(DBL), dimension(n_ps), parameter :: m_ps = (/ 138.041d0, 548.8d0, 957.5d0 /)  !masses
   real(DBL), dimension(n_ps), parameter :: g_ps = (/ 3.66d0, 2.72967d0, 3.88786d0 /) !couplings

   
   !Vectors (\rho,  \omega, \phi)

   integer, parameter :: n_v = 3  !Number of vectors 
   integer, parameter, dimension(n_v) :: iso_v = (/ 1, 0, 0 /)  !iso-spin
   real(DBL), dimension(n_v), parameter :: m_v = (/ 770.0d0, 783.9d0, 1019.5d0 /)  !masses
   real(DBL), dimension(n_v), parameter :: g_v = (/ 0.59444d0, 3.37308d0, -1.12412d0 /) !couplings
   real(DBL), dimension(n_v), parameter :: fg_v = (/ 4.81696d0, 2.33992d0, -0.51004d0 /) !f/g

   !Scalars (\epsilon)
   integer, parameter :: n_s = 1 !Number of  scalars
   integer, parameter, dimension(n_s) :: iso_s = (/ 0 /)  !iso-spin
   real(DBL), dimension(n_s), parameter :: m_s = (/ 760.0d0 /)  !masses
   real(DBL), dimension(n_s), parameter :: g_s = (/ 5.03208d0 /) !couplings

   !Parameters for two-poles approxmation of \epsilon and \rho
   integer, parameter :: n_b = 2
   real(DBL), parameter, dimension(n_b) :: m_1 = (/ 508.58d0, 628.74d0 /)
   real(DBL), parameter, dimension(n_b) :: m_2 = (/ 1043.79d0, 878.18d0 /)
   real(DBL), parameter, dimension(n_b) :: beta_1 = (/ 0.19986d0, 0.15874d0 /)
   real(DBL), parameter, dimension(n_b) :: beta_2 = (/ 0.55241d0, 0.78321d0 /)

      
   !local variables

   integer :: t1_t2, s1_s2, f_iso, t_c, i
   real(DBL) :: s12_me, so_me, y, x, b, r1, r2, r3, r4, g, g2, qls_me, x1, x2
   real(DBL), dimension(n_ps) :: V_ps
   real(DBL), dimension(n_v) :: V_v
   real(DBL), dimension(n_s) :: V_s
   real(DBL), dimension(n_b) :: V_b
   
   
   V = 0.0d0

   t1_t2 = 2*T*(T+1) - 3      !matrix element of \tau_1*\tau_2
   s1_s2 = 2*S*(S+1) - 3      !matrix element of \sigma_1*\sigma_2
   s12_me = S_12(S, J, L, LP)       !matrix element of S_12
   so_me = spin_orbit(S, J, L, LP)  !matrix element of L*S
   qls_me = quadrupole(S, J, L, LP)
   t_c = 1
   
   
   if(L /= LP) then

      t_c = 0
      s1_s2 = 0

   end if

   
   !iso-scalar exchanges

   do i = 1, n_ps
   
      f_iso = 1 + iso_ps(i)*(-1 + t1_t2)
      y = (m_ps(i)/(2*M_N))**2
      x = m_ps(i)*r/hbarc
      V = y*(s1_s2/3.0d0 + F_T(x)*s12_me)
      g2 = g_ps(i)**2
      V_ps(i) = f_iso*g2*m_ps(i)*V*yukawa(x)

   end do


   !vector exchanges

   do i = 1, n_v

      f_iso = 1 + iso_v(i)*(-1 + t1_t2)
      b = fg_v(i)
      y = (m_v(i)/(2*M_N))**2
      x = m_v(i)*r/hbarc
      g = g_v(i)
      g2 = g*g
      r1 = (1.0d0 + 0.5d0*y)*g2 + 2*y*b*g + y**2*b**2      
      r2 = y*g2 + 2*y*b*g + y*(1 + 0.5d0*y)*b**2
      r3 = 2*y*(3*g2 + 4*b*g + 3*y*b**2)
      r4 = y**2*(g2 + 8*b*g + 8*b**2) 
      V = r1*t_c + r2*(2.0d0/3.0d0*s1_s2 - F_T(x)*s12_me) - r3*F_LS(x)*so_me + r4*FQ(x)*qls_me
      
      V_v(i) = f_iso*m_v(i)*V*yukawa(x)

   end do
      

   !Scalar exchanges

   do i = 1, n_s

      f_iso = 1 + iso_s(i)*(-1 + t1_t2)
      y = (m_s(i)/(2*M_N))**2
      x = m_s(i)*r/hbarc
      g = g_s(i)
      V = (0.5d0*y - 1)*t_c - 2*y*F_LS(x)*so_me - y**2*FQ(x)*qls_me
      V_s(i) = f_iso*g**2*m_s(i)*V*yukawa(x)

   end do

   !Treatment of broad resonances

   if(L == LP) then
 
      do i = 1, n_b
         
         x1 = m_1(i)*r/hbarc
         
         x2 = m_2(i)*r/hbarc

         V_b(i) = m_1(i)*beta_1(i)*yukawa(x1) + m_2(i)*beta_2(i)*yukawa(x2)
         
      end do

   end if
      
   V = - V_ps(1) + V_ps(2) + v_ps(3) + V_v(1) - V_v(2) - V_v(3) + V_s(1) + V_b(1) + V_b(2)  
   
   
 end subroutine V_NN_NIJ


 

 elemental function yukawa(x) result(y)
   !--------------------------------------
   !PURPOSE:
   !        Computes the value exp(-x)/x
   !-------------------------------------

   real(DBL), intent(in) :: x
   real(DBL) :: y

   y = exp(-x)/x

 end function yukawa

 elemental function F_T(x) result(f)
   !----------------------------------------
   !PURPOSE:
   !       Computes F_T(x), i.e 1/3*H(x)/F(X)
   !       apperaring in the BP interaction
   !INPUT:
   !x: value of x
   !-------------------------------------------

   real(DBL), intent(in) :: x
   real(DBL):: f

   f = 1.0d0/3.0d0+1/x+1/(x*x)

 end function F_T

 elemental function F_LS(x) result(f)
   !-------------------------------------------
   !PURPOSE:
   !        Computes the radial function
   !        multiplying the LS term in th BP
   !        interaction
   !INPUT:
   !x: value of x
   !------------------------------------------
   
   real(DBL), intent(in) :: x
   real(DBL) :: f

   f = (x+1)/(x*x)

 end function F_LS
   
 elemental function FQ(x) result(f)
   !-------------------------------------------
   !PURPOSE:
   !        Computes the radial function
   !        multiplying the quadrupole term in the
   !        Nijmegen interaction
   !INPUT:
   !x: value of x
   !------------------------------------------

   real(DBL), intent(in) :: x
   real(DBL) :: f

   
   f = (x*(x + 3) + 3)/(x**4)

 end function FQ
 
 elemental function S_12(S, J, L, LP) result(S12)
   !-------------------------------------------------------------
   !PURPOSE:
   !       Computes the  tensor interaction matrix element, S_12
   !INPUT:
   !S: spin
   !J: total angular momentum
   !L: Orbital angular momentum of final state
   !LP: Orbital angular momentum of intitial state
   !--------------------------------------------------------------
   
   integer, intent(in) :: S, J, L, LP
   real(DBL) :: S12

   !local variables
   integer :: JM1, JP1
   
   S12 = 0.0d0

   if(S /= 1) return
   
   JM1 = J-1
   JP1 = J+1


   if( L == LP) then

      if(L == JM1) then

         S12 = -2.0d0*JM1/(2*J+1.0d0) 

      else if(L == J) then

         S12 = 2.0d0

      else if(L == JP1) then

         S12 = -2.0d0*(J+2.0d0)/(2*J+1.0d0)
            
      end if

   else if((L == JM1 .and. LP == JP1) .or. (L == JP1 .and. LP == JM1)) then

      S12 = 6.0d0*sqrt(J*(J+1.0d0))/(2*J+1.0d0)

   end if
   
 end function S_12
 

 elemental function spin_orbit(S, J, L, LP) result(SO)
   !----------------------------------------------------------------------
   !PURPOSE:
   !        Computes the matrix element of the spin-orbit operator L\cdot S
   !INPUT:
   !S: spin
   !J: total angular momentum
   !L: Orbital angular momentum of final state
   !LP: Orbital angular momentum of intitial state
   !-----------------------------------------------------------------------

   integer, intent(in) :: S, J, L, LP
   real(DBL) :: SO

   SO = 0.0d0

   if(L /= LP .or. L < 1 .or. S < 1) return
   
 
   if(J <= (L+S) .and. J >= abs(L-S)) then

      SO = 0.5d0*(J*(J+1)-L*(L+1)-S*(S+1))

   end if
 

 end function spin_orbit

 elemental function quadrupole(S, J, L, LP) result(Q12)
   !----------------------------------------------------------------------
   !PURPOSE:
   !        Computes the matrix element of the quadrupole spin-orbit operator
   !INPUT:
   !S: spin
   !J: total angular momentum
   !L: Orbital angular momentum of final state
   !LP: Orbital angular momentum of intitial state
   !-----------------------------------------------------------------------

   integer, intent(in) :: S, J, L, LP
   real(DBL) :: Q12

   Q12 = 0.0d0

   if((L /= LP) .or. (L == 0) .or. ((J > (L + S) .or. J < abs(L - S)))) return
 

   if(S == 0) then

      Q12 = - J*(J + 1.0d0)

   else if(L == J - 1) then

      Q12 = (J - 1.0d0)**2

   else if(L == J) then

      Q12 = 1.0d0 - J*(J + 1.0d0)


   else if(L == J + 1) then

      Q12 = (J + 2.0d0)**2

   end if

   
 end function quadrupole

 subroutine diag_rq(P, H, n, e_value, e_vector, info)
      !-----------------------------------------------------------
   !PURPOSE:
   !Solves the generalized eigenvalue problem Px = Hx,
   !using the Rayleigh quotient method
   !INPUT:
   !P : ;eft-hand side matrix
   !H : right-hand side matrix
   !n : dimension
   !INPUT/OUTPUT:
   !e_value : on input guess for the eigenvalue
   !          on output the computed eigenvalue   
   !e_vector: computed eigenvector
   !info : 0 if succesful run
   !-----------------------------------------------------------

   use omp_lib, only : omp_get_wtime
   
   integer, intent(in) :: n
   complex(DBL), dimension(n, n), intent(in) :: P, H
   complex(DBL), intent(out) :: e_value
   complex(DBL), dimension(n), intent(out) :: e_vector
   integer, intent(out) :: info
   
   !local variables

   integer, parameter :: imax = 100
   real(DBL), parameter :: eps_evec = 1.0d-6

   
   complex(DBL), dimension(n, n) :: P_copy, H_copy
   integer, dimension(n) :: ipiv
   complex(DBL), dimension(n, n) :: Ht
   complex(DBL), dimension(n, 1) :: b
   complex(DBL), dimension(n) :: tmp
   complex(DBL) :: E_old
   real(DBL) :: Ck, diff, t1, t2
   integer :: i, j, iter
   
   
   P_copy = P
   H_copy = H

   t1 = omp_get_wtime()


   call zgesv(n, n, P_copy, n, ipiv, H_copy, n, info)

   t2 = omp_get_wtime()

   write(0, *) "Time to invert P: ", t2-t1
   
   if(info /= 0) return

   !initialize e_vector to (1 0 0 ... 0)

   e_vector = 0.0d0
   
   e_vector(1) = 1.0d0


!   write(0, *) "e_vector: ", e_vector

   do iter = 1, imax

      !Compute the matrix \tilde{H} = (H-E*x)

      do j = 1, n
   
         do i = 1, n
            
            Ht(i, j) = H_copy(i, j)

            if(i == j) Ht(i, j) = Ht(i, j) - e_value

         end do

      end do
      
      !Compute the LU factorization

      t1 = omp_get_wtime()

      
      call zgetrf(n, n, Ht, n, ipiv, info)

      if(info /= 0) return
      
      t2 = omp_get_wtime()

      write(0, *) "Time to compute LU: ", t2-t1
      
      !Compute \tilde{H}^{-1} x

      b(:, 1) = e_vector
      tmp = e_vector
 
      t1 = omp_get_wtime()

      
      call zgetrs('N', n, 1, Ht, n, ipiv, b, n, info)

      t2 = omp_get_wtime()

      write(0, *) "Time to solve eqs: ", t2-t1
 
      t1 = omp_get_wtime()

      
      e_vector = b(:, 1)
      
      !Normalize
      
      Ck = 0.0d0
      
      do i = 1, n
         
         Ck = Ck + abs(e_vector(i))**2
         
      end do

      Ck = sqrt(Ck)
      
      e_vector = e_vector/Ck
      

   !   write(0, *) "Ck : ", Ck
      
      !Compute new eigenvalue
      
      E_old = E_value
      
      E_value = 0.0d0
      
      do j = 1, n
         
         do i = 1, n
            
            E_value = E_value + conjg(e_vector(i))*H_copy(i, j)*e_vector(j)
         
         end do
         
      end do

      
      !Compute error
      
      diff = 0.0d0

      do i = 1, n
         
         diff = diff + abs(e_vector(i) - tmp(i))**2
         
      end do
      
      diff = sqrt(diff)

      t2 = omp_get_wtime()

      write(0, *) "Time for the rest: ", t2-t1
      
      print *, iter, E_value, diff, abs(E_value-E_old)
      
      if(diff < eps_evec) exit


   end do

 end subroutine diag_rq


 subroutine diag_rq_new(P, H, n, e_value, e_vector, info)
      !-----------------------------------------------------------
   !PURPOSE:
   !Solves the generalized eigenvalue problem Px = Hx,
   !using the Rayleigh quotient method
   !INPUT:
   !P : ;eft-hand side matrix
   !H : right-hand side matrix
   !n : dimension
   !INPUT/OUTPUT:
   !e_value : on input guess for the eigenvalue
   !          on output the computed eigenvalue   
   !e_vector: computed eigenvector
   !info : 0 if succesful run
   !-----------------------------------------------------------

   use omp_lib, only : omp_get_wtime
   
   integer, intent(in) :: n
   complex(DBL), dimension(n, n), intent(in) :: P, H
   complex(DBL), intent(out) :: e_value
   complex(DBL), dimension(n), intent(out) :: e_vector
   integer, intent(out) :: info
   
   !local variables

   integer, parameter :: imax = 100
   real(DBL), parameter :: eps_evec = 1.0d-6

   
   complex(DBL), dimension(n, n) :: P_copy, H_copy
   integer, dimension(n) :: ipiv
   complex(DBL), dimension(n, n) :: Ht
   complex(DBL), dimension(n, 1) :: b
   complex(DBL), dimension(n) :: tmp
   complex(DBL), dimension(n) :: y
   complex(DBL) :: E_old
   real(DBL) :: Ck, diff, t1, t2
   integer :: i, j, iter
   
   
   P_copy = P
   H_copy = H

   t1 = omp_get_wtime()


   
   call zgesv(n, n, P_copy, n, ipiv, H_copy, n, info)

   t2 = omp_get_wtime()

   write(0, *) "Time to invert P: ", t2-t1
   
   if(info /= 0) return

   !initialize e_vector to (1 0 0 ... 0)

   e_vector = 0.0d0
   
   e_vector(1) = 1.0d0


!   write(0, *) "e_vector: ", e_vector

   do iter = 1, imax

      !Compute the matrix \tilde{H} = (H-E*x)

      do j = 1, n
   
         do i = 1, n
            
            Ht(i, j) = H(i, j) - e_value*P(i, j)

 !           if(i == j) Ht(i, j) = Ht(i, j) - e_value

         end do

      end do

      y = 0.0d0
      
      do j = 1, n

         do i = 1, n

            y(i) = y(i) + P(i, j)*e_vector(j)

         end do

      end do
      
      !Compute the LU factorization

      t1 = omp_get_wtime()

      
      call zgetrf(n, n, Ht, n, ipiv, info)

      if(info /= 0) return
      
      t2 = omp_get_wtime()

      write(0, *) "Time to compute LU: ", t2-t1
      
      !Compute \tilde{H}^{-1} x

      b(:, 1) = y
      tmp = e_vector
 
      t1 = omp_get_wtime()

      
      call zgetrs('N', n, 1, Ht, n, ipiv, b, n, info)

      t2 = omp_get_wtime()

      write(0, *) "Time to solve eqs: ", t2-t1
 
      t1 = omp_get_wtime()

      
      e_vector = b(:, 1)
      
      !Normalize
      
      Ck = 0.0d0
      
      do i = 1, n
         
         Ck = Ck + abs(e_vector(i))**2
         
      end do

      Ck = sqrt(Ck)
      
      e_vector = e_vector/Ck
      

   !   write(0, *) "Ck : ", Ck


      
      !Compute new eigenvalue
      
      E_old = E_value
      
      E_value = 0.0d0
      
      do j = 1, n
         
         do i = 1, n
            
            E_value = E_value + conjg(e_vector(i))*H_copy(i, j)*e_vector(j)
         
         end do
         
      end do

      
      !Compute error
      
      diff = 0.0d0

      do i = 1, n
         
         diff = diff + abs(e_vector(i) - tmp(i))**2
         
      end do
      
      diff = sqrt(diff)

      t2 = omp_get_wtime()

      write(0, *) "Time for the rest: ", t2-t1
      
      print *, iter, E_value, diff, abs(E_value-E_old)
      
      if(diff < eps_evec) exit


   end do

 end subroutine diag_rq_new

 subroutine get_lu_dcmp(A, n)
   !----------------------------------------------
   !PURPOSE:
   !Computes the LU decomposition for the given matrix
   !INPUT:
   !n : dimension of the matrix
   !IN/OUT:
   !A : on input the matrix to be decomposed
   !    on output overwritten with the LU decomposition
   !-----------------------------------------------------

   integer, intent(in) :: n
   complex(DBL), dimension(n, n), intent(inout) :: A

   !local variables

   integer :: i, j, k
   complex(DBL) :: tmp
   
   do j = 1, n

      do i = 1, j

         tmp = A(i, j)
         
         do k = 1, i-1

            tmp = tmp - A(i, k)*A(k, j)

         end do

         A(i, j) = tmp

      end do

      do i = j+1, N

         tmp = A(i, j)

         do k = 1, j-1

            tmp = tmp - A(i, k)*A(k, j)

         end do

         A(i, j) = tmp/A(j, j)

      end do

   end do


 end subroutine get_lu_dcmp


 subroutine solve_eq_syst(A, b, n)
   !---------------------------------------------
   !PURPOSE:
   !Given the LU decomposition of A, solves Ax=b
   !INPUT:
   !n : dimension
   !A : the LU decomposition of the matrix
   !IN/OUT:
   !b : on input, the right-hand side of the equation
   !    on output, the solution
   !--------------------------------------------------

   integer, intent(in) :: n
   complex(DBL), dimension(n, n), intent(in) :: A
   complex(DBL), dimension(n), intent(inout) :: b

   !local variables

   integer :: i, j
   complex(DBL) :: tmp

   ! Solve L*y = b
   

   do i = 1, n

      tmp = b(i)

      do j = 1, i-1

         tmp = tmp - A(i, j)*b(j)

      end do

      b(i) = tmp

   end do

   !Solve U*x = y

   do i = n, 1, -1

      tmp = b(i)

      do j = i+1, n

         tmp = tmp - A(i, j)*b(j)

      end do

      b(i) = tmp/A(i, i)

   end do
   
 end subroutine solve_eq_syst

 subroutine compute_inv(A, n)
   !-----------------------------------------
   !PURPOSE :
   !Computes the inverse of the given matrix
   !INPUT :
   !n : dimension
   !IN/OUT:
   !A : on input the matrix to be inverted
   !  : on output, replaced by the inverse
   !----------------------------------------

   integer, intent(in) :: n
   complex(DBL), dimension(n, n), intent(inout) :: A

   !local variables

   integer :: i, j
   complex(DBL), dimension(n, n) :: LU
   
   LU = A

   !Compute LU decomposition
   
   call get_lu_dcmp(LU, n)
   
   do i = 1, n

      do j = 1, n

         A(j, i) = 0.0d0

      end do

      A(i, i) = 1.0d0
      
      call solve_eq_syst(LU, A(:, i), n)

   end do

   
 end subroutine compute_inv

 
! subroutine lu_band(A, n, k, L, U)
   !----------------------------------------------
   !PURPOSE:
   !Computes the LU factorization for the given matrix
   !stored in the band-storage scheme
   !INPUT:
   !A : input matrix of size 
   !n : number of columns in A
   !k : number of super-diagonals above and below the
   !    main diagonal
   !OUTPUT:
   !L : lower-triangular matrix
   !U : upper-triangular matrix
   !----------------------------------------------

 !  integer, intent(in) :: n, k
 !  complex(DBL), dimension(2*k+1, n), intent(in) :: A
 !  complex(DBL), dimension(k+1, n), intent(out) :: L, U

   !local variables

!   integer :: r, s, ip, i, t

!   complex(DBL), dimension(:), allocatable :: k_array, m_array 

!   r = min(n, 2*k+1)

!   allocate(k_array(1:r, 1:r), m_array(1:r, 1:r))


!   k_array = 0.0d0

   
!   do ip = 1, r - 1

!      k_array(1, ip) = A(1, ip)

!      m_array(1, ip) = A(ip+1, 1)/k_array(1, 1)
   
!   end do

!   k_array(1, r) = A(1, r)

   !i'==1

   
!   do s = 1, r

!      k(s, 1) = A(s, s)

!      do t = 1, r - 1

!         k(s, 1) = k(s, 1) - m_array(s-t, t +1)*k_array(s-t, t+1)

!      end do

!   end do


   
   
!   do ip = 1, r - 1

!      do s = 1, r

!         k_array(s, i) = 
         

   
   
  
   
   
   
! end subroutine lu_band
   
   
 subroutine diag_zgeev(P, H, n, e_values, e_vectors, info)
   !-----------------------------------------------------------
   !PURPOSE:
   !Solves the generalized eigenvalue problem Px = Hx,
   !using the LAPACK routine zgeev
   !INPUT:
   !P : left-hand side matrix
   !H : right-hand side matrix
   !n : dimension
   !OUTPUT:
   !e_values : computed eigenvalues
   !e_vectors: computed eigenvectors
   !info : 0 if succesfull run
   !-----------------------------------------------------------

   
   integer, intent(in) :: n
   complex(DBL), dimension(n, n), intent(in) :: P, H
   complex(DBL), dimension(n), intent(out) :: e_values
   complex(DBL), dimension(n, n), intent(out) :: e_vectors
   integer, intent(out) :: info
   
   !local variables
   complex(DBL), dimension(n, n) :: P_copy, H_copy
   integer, dimension(n) :: ipiv
   complex(DBL), dimension(n) :: w
   complex(DBL), dimension(1, n) :: vl
   complex(DBL), dimension(1) :: tmp
   real(DBL), dimension(2*n) :: rwork
   integer :: lwork
   complex(DBL), dimension(:), allocatable :: work
   integer :: i, j
   complex(DBL) :: lambda
   
   info = 0

   P_copy = P
   H_copy = H

   e_values = 0.0d0

   e_vectors = 0.0d0
   
   !Compute P^{-1}*H
   
   call zgesv(n, n, P_copy, n, ipiv, H_copy, n, info)
   
   if(info /= 0) return
   
   
   !Diagonalize H
    
    call zgeev('N', 'V', n, H_copy, n, e_values, vl, 1, e_vectors, n, tmp, -1, rwork, info)

    
    
    lwork = int(tmp(1))

    
    
    allocate(work(lwork))

    call zgeev('N', 'V', n, H_copy, n, e_values, vl, 1, e_vectors, n, work, lwork, rwork, info)

    deallocate(work)

    if(info /= 0) return

    
    !sort eigenvalues and eigenvectors

    do i = 1, n
       do j = 1, i
          if(real(e_values(j), DBL) > real(e_values(i), DBL)) then
             lambda = e_values(i)
             e_values(i) = e_values(j)
             e_values(j) = lambda
             w = e_vectors(:, i)
             e_vectors(:, i) = e_vectors(:, j)
             e_vectors(:, j) = w
          end if
       end do
    end do
    


   
  end subroutine diag_zgeev



 subroutine diag_dgeev(P, H, n, e_values, e_vectors, info)
   !-----------------------------------------------------------
   !PURPOSE:
   !Solves the generalized eigenvalue problem Px = Hx,
   !using the LAPACK routine dgeev
   !INPUT:
   !P : ;eft-hand side matrix
   !H : right-hand side matrix
   !n : dimension
   !OUTPUT:
   !e_values : computed eigenvalues
   !e_vectors: computed eigenvectors
   !info : 0 if succesfull run
   !-----------------------------------------------------------
   
   integer, intent(in) :: n
   real(DBL), dimension(n, n), intent(in) :: P, H
   complex(DBL), dimension(n), intent(out) :: e_values
   complex(DBL), dimension(n, n), intent(out) :: e_vectors
   integer, intent(out) :: info
   
   !local variables
   real(DBL), dimension(n, n) :: P_copy, H_copy
   integer, dimension(n) :: ipiv
   real(DBL), dimension(n) :: wr, wi
   real(DBL), dimension(1, n) :: vl
   real(DBL), dimension(n, n) :: vr
   real(DBL), dimension(1) :: tmp
   integer :: lwork
   real(DBL), dimension(:), allocatable :: work
   integer :: i, j
   complex(DBL) :: lambda
   complex(DBL), dimension(n) :: w
   
   info = 0

   P_copy = P
   H_copy = H

   e_values = 0.0d0

   e_vectors = 0.0d0
   
   !Compute P^{-1}*H
   
   call dgesv(n, n, P_copy, n, ipiv, H_copy, n, info)
   
   if(info /= 0) return
   
   
   !Diagonalize H
    
    call dgeev('N', 'V', n, H_copy, n, wr, wi, vl, 1, vr, n, tmp, -1, info)

    
    
    lwork = int(tmp(1))

    
    
    allocate(work(lwork))

    call dgeev('N', 'V', n, H_copy, n, wr, wi, vl, 1, vr, n, work, lwork, info)

    deallocate(work)

    if(info /= 0) return
 

    do i = 1, n

       e_values(i) = cmplx(wr(i), wi(i), DBL)

    end do

    
    do i = 1, n

       if(abs(wi(i)) < 1.0d-15) then
          !real eigenvalue
          
          e_vectors(:, i) = vr(:, i)
       else
          if(i < n) then
             do j = 1, n
                e_vectors(j, i) = cmplx(vr(j, i), vr(j, i + 1), DBL)
                e_vectors(j, i + 1) = cmplx(vr(j, i), -vr(j, i + 1), DBL)
             end do
          end if
       end if

    end do

    
    !sort eigenvalues and eigenvectors

    do i = 1, n
       do j = 1, i
          if(real(e_values(j), DBL) > real(e_values(i), DBL)) then
             lambda = e_values(i)
             e_values(i) = e_values(j)
             e_values(j) = lambda
             w = e_vectors(:, i)
             e_vectors(:, i) = e_vectors(:, j)
             e_vectors(:, j) = w
          end if
       end do
    end do
    


   
  end subroutine diag_dgeev
   
 
 
end module bstate_cc_mod
