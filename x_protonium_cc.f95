program x_bstate_cc

  use bstate_cc_mod, only : solve_cc

  character(len=6) :: c_mult
  integer :: ncol, nintervals_pp, nintervals_mm, nintervals_nn, n_quad, diag_mod, verbose, ibc
  real(8) :: m1, m2, rmax_pp, rmax_mm, rmax_nn, alpha_pp, rc_0, rc_1, lambda_0, lambda_1, E0_r, E0_i, &
       rstep_gamma, rmax_gamma
  complex(8) :: E0

  namelist /INPUT/ c_mult, nintervals_pp, nintervals_mm, nintervals_nn, rmax_pp, rmax_mm, rmax_nn, alpha_pp, rc_0, &
       rc_1, lambda_0, lambda_1, ncol, E0_r, E0_i, n_quad, diag_mod, rstep_gamma, rmax_gamma
  read(*, nml = INPUT)

  print *
  print *, "==============================================="
  print *, "INPUT:"
  print *
  print *, "c_mult: ", c_mult
  print *, "nintervals_pp, nintervals_mm, nintervals_nn: ", nintervals_pp, nintervals_mm, nintervals_nn
  print *, "rmax_pp, rmax_mm, rmax_nn: ", rmax_pp, rmax_mm, rmax_nn
  print *, "alpha_pp: ", alpha_pp
  print *, "rc_0, rc_1: ", rc_0, rc_1
  print *, "lambda_0, lambda_1: ", lambda_0, lambda_1
  print *, "ncol: ", ncol
  print *, "E0_r, E0_i: ", E0_r, E0_i
  print *, "n_quad: ", n_quad
  print *, "diag_mod: ", diag_mod
  print *, "rstep_gamma: ", rstep_gamma
  print *, "rmax_gamma: ", rmax_gamma
  print *
  print *, "==============================================="
  print *
  
  E0 = cmplx(E0_r, E0_i, 8)
  

  
!  c_mult = '1S0'

!  T = 0

!  nintervals = 10

!  m1 = 938.28d0
!  m2 = 938.28d0 
!  m2 = 763.0d0
!  rmax = 5.0d0
!  rc = 1.5d0
!  lambda = 0.0d0

  verbose = 0

  ibc = 0
  
  call solve_cc(c_mult, nintervals_pp, nintervals_mm, nintervals_nn, rmax_pp, rmax_mm, rmax_nn, alpha_pp, rc_0, &
       rc_1, lambda_0, lambda_1, ncol, E0, n_quad, diag_mod, verbose, ibc, rstep_gamma, rmax_gamma)

  
end program x_bstate_cc
