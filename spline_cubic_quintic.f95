module spline_mod_cubic_quintic
  !Implementation of the cubic quintic spline functions, ie. piece-wise
  !third and fifth order polynomials respectively
  !It should be noticed that all vectors start at index 0 and 1.
  !Routines dedicated for the case where points corresponding to a and b are removed
  
  private
  
  
  integer, parameter :: DBL = 8
  real(DBL), parameter :: EPS = 1.0d-15

  real(DBL), dimension(:, :), allocatable, public :: xmesh   !mesh points for each channel
  real(DBL), dimension(:, :), allocatable, public :: xpoints !collocation points for each channel
  integer, dimension(:), allocatable, public :: nx_array !number of intervals  for each channel
  
  
!  integer :: nxcolloc
!  integer :: statuspw = 1
!  real(DBL) :: xmin, xmax

  public :: S_x, Sp_x, Spp_x, Stilde_x, Stilde_p_x, Stilde_pp_x, get_xlimits, &
       colloc_gauss, sub_interv_equidist

  
contains

!  subroutine spline_init(n_col, n_xintervals, ax, bx, alpha_x)
    !---------------------------------------------------------------------------------
    !PURPOSE:
    !Performs the initial tasks such as allocating the dynamical arrays, etc
    !INPUT:
    !n_col : 2 => cubic splines, 3 => quintic splines
    !n_xintervals, n_yintervals: number of intervals for x and y
    !ax, bx: start, end point of the interval (x)
    !alpha_x : scaling parameters
    !
    !----------------------------------------------------------------------------------

!    integer, intent(in):: n_col, n_xintervals
!    real(DBL), intent(in):: ax, bx
!    real(DBL), optional, intent(in):: alpha_x

!    if(n_col /= 2 .and. n_col /= 3 ) then
!       write(0, *) 'Error in spline_init: Invalid value of n_col'
!       stop
!    end if
    
!    xmin = ax
!    xmax = bx

!    nx = n_xintervals

!    if(n_col == 2) then

!       nxcolloc = 2*nx+1

!    else 
       
!       nxcolloc = 3*nx+2
       
!    end if
 
        
 !   if(statuspw == 0) then
 !      deallocate(xpoints_1d, xcolloc)
 !   end if

!    allocate(xpoints_1d(0:nx), xcolloc(0:nxcolloc))
    
    
!    if(present(alpha_x)) then
!       call sub_interv_equidist(ax, bx, xpoints_1d, alpha_x)
!    else
!       call sub_interv_equidist(ax, bx, xpoints_1d, 1.0d0)
!    end if

    
!    call colloc_gauss(n_col, xpoints_1d, xcolloc, mode_xlimits)

!  end subroutine spline_init

!  subroutine spline_init_new(n_col, nxintervals_1, ax1, bx1, nxintervals_2, bx2, alpha_x)
    !---------------------------------------------------------------------------------
    !PURPOSE:
    !Performs the initial tasks such as allocating the dynamical arrays, etc
    !This version is for two a mesh contain two parts, namely, [ax1, bx1] and [bx1, bx2]
    !INPUT:
    !n_col : 2 => cubic splines, 3 => quintic splines
    
    !nxintervals_1, nxintervals_2: number of intervals for regions 1 and 2
    !a1, bx1: start, end point of the first region
    !bx2 : end point of the second region
    !alpha_x : scaling parameters
    !
    !----------------------------------------------------------------------------------

!    integer, intent(in):: n_col, nxintervals_1, nxintervals_2
!    real(DBL), intent(in):: ax1, bx1, bx2
!    real(DBL), optional, intent(in):: alpha_x
!    integer :: nx
    
    
!    if(n_col /= 2 .and. n_col /= 3 ) then
!       write(0, *) 'Error in spline_init: Invalid value of n_col'
!       stop
!    end if
    
!    xmin = ax1
!    xmax = bx2

!    nx = nxintervals_1 + nxintervals_2

 !   if(n_col == 2) then

 !      nxcolloc = 2*nx+1

 !   else 
       
 !      nxcolloc = 3*nx+2
       
 !   end if
       
 
        
 !   if(statuspw == 0) then
 !      deallocate(xpoints_1d, xcolloc)
 !   end if

!    allocate(xpoints_1d(0:nx), xcolloc(0:nxcolloc))
    
    
!    if(present(alpha_x)) then
!       call sub_interv_equidist(ax1, bx1, xpoints_1d(0:nxintervals_1), alpha_x)
!       call sub_interv_equidist(bx1, bx2, xpoints_1d(nxintervals_1:nx), alpha_x)
    
!    else
    !   call sub_interv_equidist(ax, bx, xpoints_1d, 1.0d0)
!       call sub_interv_equidist(ax1, bx1, xpoints_1d(0:nxintervals_1), 1.0d0)
!       call sub_interv_equidist(bx1, bx2, xpoints_1d(nxintervals_1:nx), 1.0d0)
    
!    end if

    
!    call colloc_gauss(n_col, xpoints_1d, xcolloc, mode_xlimits)

!  end subroutine spline_init_new


!  subroutine spline_clean()
    !------------------------------------------
    !PURPOSE: Deallocates the dynamical arrays
    !-------------------------------------------
    
 !   deallocate(xpoints_1d, xcolloc)

!  end subroutine spline_clean
  
    
  pure  subroutine sub_interv_equidist(a, b, points_1d, alpha)
    !--------------------------------------------------------------------------
    !PURPOSE: Partitions the interval [a,b] into subintervals and
    !         stores the data in the the given arrays
    !INPUT:
    !a, b: limits of the interval
    !alpha: scaling parameter, h_i=alpha*h_{i-1}
    !OUTPUT:
    !points_1d: one-dimensional double array, contains after a call the  
    !           abscissas for all intervals listed after each other
    !-----------------------------------------------------------------------------
      
    real(DBL), intent(in)::a, b, alpha
    real(DBL), dimension(0:), intent(inout)::points_1d
    integer:: npoints, n
    real(DBL):: h, ak

     ak = a
     npoints = size(points_1d, 1)
     h = (b-a)/(npoints-1)
     
     
     if(abs(alpha-1.0d0) > EPS) then
        h = (b-a)*(alpha-1.0d0)/(alpha**(npoints-1)-1.0d0)
     end if
     
     points_1d(0) = a
     do n = 1, npoints-1
        points_1d(n) = points_1d(n-1)+h
        h = h*alpha
     end do
     points_1d(npoints-1) = b


   end subroutine sub_interv_equidist

 
   function compute_spline_val(n_col, j, x, points) result(s)
     !-------------------------------------------------
     !PURPOSE: 
     !Computes the value of the spline S_j(x)
     !INPUT:
     !n_col : 2 => cubic splines, 3 => quintic splines
     !j: enumeration of the spline
     !x: value of the spline
     !points: mesh points defining the splines
     !--------------------------------------------------
     
     integer, intent(in):: n_col, j
     real(DBL), intent(in):: x
     real(DBL), dimension(0:), intent(in):: points
     real(DBL):: xi, x_min, x_max, h, t
     real(DBL):: s
     integer:: n, i, i1

     s = 0.0d0

     if(n_col /= 2 .and. n_col /= 3) return 
     
     
     n = size(points, 1)
     
     i = j/n_col

     if(j < 0 .or. i >= n ) return


     i1 = mod(j, n_col)

     x_min = points(max(i-1, 0))
     x_max = points(min(n-1, i+1))
  
     if(x > x_max .or. x < x_min ) return
       
     xi = points(i)

     
     if( (i == 0 .and.  x < xi) .or. (i == n-1 .and. x > xi) ) return 


     if(i == 0) then
        h = xi - points(i+1)
        t = (x-points(i+1))/h

     else if(i == n-1) then

        h = xi - points(i-1)
        t = (x-points(i-1))/h

     else 


        if(x < xi) then
           h = xi - points(i-1)
           t = (x-points(i-1))/h
           
        else
           h = xi - points(i+1)
           t = (x-points(i+1))/h
        
        end if

     end if

        
     if(i1 == 0) then

        if(n_col == 2) then

           s = t*t*(3-2*t)

        else 
           
           s = t**3*(3*(t-1)*(2*(t-1)-1) + 1)

        end if

     end if

     if(i1 == 1) then

        if(n_col == 2) then

           s = -t*t*(1-t)*h

        else
        
           s = -t**3*(1-t)*(4-3*t)*h

        end if

     end if

     if(i1 == 2) then

        s = 0.5d0*t**3*(1-t)**2*h**2

     end if
     
   end function compute_spline_val


   pure function compute_spline_1st_derivative(n_col, j, x, points) result(s)
     !-------------------------------------------------
     !PURPOSE: 
     !Computes the value of the spline derivative S'_j(x)
     !INPUT:
     !n_col : 2 => cubic splines, 3 => quintic splines
     !j: enumeration of the spline
     !x: value of the spline
     !points: mesh points defining the splines
     !--------------------------------------------------
     
     integer, intent(in):: n_col, j
     real(DBL), intent(in):: x
     real(DBL), dimension(0:), intent(in):: points
     real(DBL):: xi, x_min, x_max, h, t
     real(DBL):: s
     integer:: n, i, i1

     s = 0.0d0

     if(n_col /= 2 .and. n_col /= 3) return 
   
     n = size(points, 1)

     i = j/n_col

     if(j < 0 .or. i >= n ) return

     i1 = mod(j, n_col)
          
     x_min = points(max(i-1, 0))
     x_max = points(min(n-1, i+1))
  
     if(x > x_max .or. x < x_min ) return
       
     xi = points(i)

     
     if( (i == 0 .and.  x < xi) .or. (i == n-1 .and. x > xi) ) return 


     if(i == 0) then
        h = xi - points(i+1)
        t = (x-points(i+1))/h

     else if(i == n-1) then
        
        h = xi - points(i-1)
        t = (x-points(i-1))/h

     else

     
        if(x < xi) then
           h = xi - points(i-1)
           t = (x-points(i-1))/h
           
        else
           h = xi - points(i+1)
           t = (x-points(i+1))/h

        end if

     end if

        
     if(i1 == 0) then

        if(n_col == 2) then

           s = 6*t*(1-t)/h

        else
        
           s = 30*t*t*(1-t)**2/h

        end if
           
        return

     end if

     if(i1 == 1) then

        if(n_col == 2) then

           s = t*(-2+3*t)

        else 
        
           s = t*t*(t*(-15*t+28)-12)

        end if

     end if

     if(i1 == 2) then

        s = -0.5d0*t*t*(1-t)*(5*t-3)*h


     end if
     
   end function compute_spline_1st_derivative
   
   function compute_spline_2nd_derivative(n_col, j, x, points) result(s)
     !-------------------------------------------------
     !PURPOSE: 
     !Computes the value of the quintic spline derivative S'_j(x)
     !INPUT:
     !n_col : 2 => cubic splines, 3 => quintic splines
     !j: enumeration of the spline
     !x: value of the spline
     !points: mesh points defining the splines
     !--------------------------------------------------
     
     integer, intent(in):: n_col, j
     real(DBL), intent(in):: x
     real(DBL), dimension(0:), intent(in):: points
     real(DBL):: xi, x_min, x_max, h, t
     real(DBL):: s
     integer:: n, i, i1

     s = 0.0d0

     if(n_col /= 2 .and. n_col /= 3) return
     
     n = size(points, 1)

     i = j/n_col

     if(j < 0 .or. i >= n ) return


     i1 = mod(j, n_col)
          
     x_min = points(max(i-1, 0))
     x_max = points(min(n-1, i+1))
  
     if(x > x_max .or. x < x_min ) return
       
     xi = points(i)

     
     if( (i == 0 .and.  x < xi) .or. (i == n-1 .and. x > xi) ) return 

     if(i == 0) then
        h = xi - points(i+1)
        t = (x-points(i+1))/h

     else if(i == n-1) then

        h = xi - points(i-1)
        t = (x-points(i-1))/h

     else
        
        if(x < xi) then
           h = xi - points(i-1)
           t = (x-points(i-1))/h
           
        else
           h = xi - points(i+1)
           t = (x-points(i+1))/h

        end if

     end if
        
     if(i1 == 0) then

        if(n_col == 2) then

           s = 6*(1-2*t)/(h*h)

        else 
        
           s = 60*t*(1-t)*(1-2*t)/(h*h)

        end if

     end if

     if(i1 == 1) then

        if(n_col == 2) then

           s = 2*(-1 + 3*t)/h

        else 
           
           s = 12*t*(1-t)*(5*t-2)/h
           
        end if

     end if

     if(i1 == 2) then

        s = t*(2*t*(5*t-6)+3)

     end if
     
   end function compute_spline_2nd_derivative

   pure subroutine colloc_gauss(n_col, points_1d, cpoints)
     !------------------------------------------------------------
     !PURPOSE:
     !Computes and stores the collocation points for each interval,
     !by computing the routine for the cubic or quintic splines
     !The points are of the Gauss-Legendre type
     !INPUT:
     !points_1d: abscissas defining the intervals
     !OUTPUT:
     !cpoints: the computed collocation points
     !-------------------------------------------------------------

     integer, intent(in) :: n_col
     real(DBL), dimension(0:), intent(in) :: points_1d
     real(DBL), dimension(0:), intent(out) :: cpoints

     if(n_col /= 2 .and. n_col /= 3) return
     
     if(n_col == 2) &
          call colloc_gauss_cubic(points_1d, cpoints)

     if(n_col == 3) &

          call colloc_gauss_quintic(points_1d, cpoints)

     
   end subroutine colloc_gauss

     

   pure subroutine colloc_gauss_cubic(points_1d, cpoints)
     !------------------------------------------------------------
     !PURPOSE:
     !Computes and stores the collocation points for each interval,
     !for the cubic splines
     !The points are of the Gauss-Legendre type
     !INPUT:
     !points_1d: abscissas defining the intervals
     !OUTPUT:
     !cpoints: the computed collocation points
     !-------------------------------------------------------------

     !Points for the  2-point Gauss-Legendre rule
     real(DBL), dimension(2), parameter:: gpoints = &
          (/ -0.577350269189626d0, 0.577350269189626d0 /)
     real(DBL), dimension(0:), intent(in) :: points_1d
     real(DBL), dimension(0:), intent(out) :: cpoints
     integer:: n, i, npoints
     real(DBL):: a, b

      
     npoints = size(points_1d, 1)
     
     n = 0
     
     do i = 0, npoints-2
        a = points_1d(i)
        b = points_1d(i+1)

        cpoints(n) = 0.5d0*((b-a)*gpoints(1)+(b+a))    !transformation [-1,1] -> [a,b]
        n = n+1
        cpoints(n) = 0.5d0*((b-a)*gpoints(2)+(b+a))    !transformation [-1,1] -> [a,b]
        n = n+1
        
     end do
        
   end subroutine colloc_gauss_cubic




   
   
   pure subroutine colloc_gauss_quintic(points_1d, cpoints)
     !------------------------------------------------------------
     !PURPOSE:
     !Computes and stores the collocation points for each interval
     !The points are of the Gauss-Legendre type
     !INPUT:
     !points_1d: abscissas defining the intervals
     !OUTPUT:
     !cpoints: the computed collocation points
     !-------------------------------------------------------------
    
     !Points for the  2-point Gauss-Legendre rule
     real(DBL), dimension(3), parameter:: gpoints = &
          (/ -0.774596669241483D0, 0.0d0, 0.774596669241483D0  /)
     real(DBL), dimension(0:), intent(in) :: points_1d
     real(DBL), dimension(0:), intent(out) :: cpoints
     integer:: n, i, npoints
     real(DBL):: xm, a, b

      
     npoints = size(points_1d, 1)

     xm = points_1d(npoints/2)
     
     n = 0

     do i = 0, npoints-2
        a = points_1d(i)
        b = points_1d(i+1)

        if(i == npoints/2) then
           cpoints(n) = xm
           n = n +1
        end if

        cpoints(n) = 0.5d0*((b-a)*gpoints(1)+(b+a))    !transformation [-1,1] -> [a,b]
        n = n+1
        cpoints(n) = 0.5d0*((b-a)*gpoints(2) +(b+a))
        n = n+1
        cpoints(n) = 0.5d0*((b-a)*gpoints(3)+(b+a))    !transformation [-1,1] -> [a,b]
        n = n+1
        
     end do

 !    cpoints(n) = points_1d(npoints/2)

   end subroutine colloc_gauss_quintic


   

!   pure subroutine get_xcolloc(v)
     !------------------------------------
     !PURPOSE:
     !Returns the collocation points for x
     !OUTPUT:
     !v: the collocation points
     !-------------------------------------

 !    real(DBL), dimension(0:), intent(out):: v

 !    v = 0.0d0

  !   v(0:(ixmax-ixmin)) = xcolloc(ixmin:ixmax)
     
 !  end subroutine get_xcolloc

   
 !  pure subroutine get_xmesh(v)
     !---------------------------------------
     !PURPOSE:
     !Returns the mesh points for x
     !OUTPUT:
     !v: the mesh points
     !---------------------------------------
     
  !   real(DBL), dimension(0:), intent(out):: v

!     v(0:nx) = xpoints_1d
     
!   end subroutine get_xmesh

     
   function S_x(n_col, ic, j, x) result(S)
     !-------------------------------------
     !PURPOSE: 
     !Computes the value of S_j(x)
     !INPUT:
     !n_col : 2 => cubic splines, 3 => quintic splines
     !ic: index of the channel
     !j: enumeration of the spline
     !x: value of the point
     !-------------------------------------
     
     integer, intent(in):: n_col, ic, j
     real(DBL), intent(in):: x
     integer:: k, nx, nxcolloc, imax
     real(DBL):: S

     S = 0.0d0

  !   k = ixmin+j

     k = j + 1
     nx = nx_array(ic)
     nxcolloc = n_col*(nx + 1) - 1
     imax = nxcolloc - 1
     
     if(k <= imax) then
        if(k >= n_col*nx .and. imax < nxcolloc) k = k+1

        S = compute_spline_val(n_col, k, x, xmesh(ic, 0:nx))

     end if
        
           
   end function S_x


   pure function Sp_x(n_col, ic, j, x) result(S)
     !-------------------------------------
     !PURPOSE: 
     !Computes the value of S'_j(x)
     !INPUT:
     !n_col : 2 => cubic splines, 3 => quintic splines
     !ic: index of the channel
     !j: enumeration of the spline
     !x: value of the point
     !-------------------------------------
     
     integer, intent(in):: n_col, ic, j
     real(DBL), intent(in):: x
     integer:: k, nx, nxcolloc, imax
     real(DBL):: S

     S = 0.0d0

  !   k = ixmin+j

     k = j + 1
     nx = nx_array(ic)
     nxcolloc = n_col*(nx + 1) - 1
     imax = nxcolloc - 1
     
     if(k <= imax) then
        if(k >= n_col*nx .and. imax < nxcolloc) k = k+1
        S = compute_spline_1st_derivative(n_col, k, x, xmesh(ic, 0:nx))

     end if
             
   end function Sp_x


   function Spp_x(n_col, ic, j, x) result(S)
     !-------------------------------------
     !PURPOSE: 
     !Computes the value of S''_j(x)
     !INPUT:
     !n_col : 2 => cubic splines, 3 => quintic splines
     !ic: index of the channel
     !j: enumeration of the spline
     !x: value of the point
     !-------------------------------------
     
     integer, intent(in):: n_col, ic, j
     real(DBL), intent(in):: x
     integer:: k, nx, nxcolloc, imax
     real(DBL):: S

     S = 0.0d0

     k = j + 1
     nx = nx_array(ic)
     nxcolloc = n_col*(nx + 1) - 1
     imax = nxcolloc - 1
     
     if(k <= imax) then
        if(k >= n_col*nx .and. imax < nxcolloc) k = k+1
        S = compute_spline_2nd_derivative(n_col, k, x, xmesh(ic, 0:nx))

     end if
           
   end function Spp_x


   function Stilde_x(n_col, ic, j, x, ld) result(S)
     !-------------------------------------
     !PURPOSE: 
     !Computes the value of \tilde{S}_j(x)=S_j(x)*ld*S_{j+1}
     !INPUT:
     !n_col : 2 => cubic splines, 3 => quintic splines
     !ic: index of the channel
     !j : enumeration of the spline
     !x : value of the point
     !ld : logarithmic derivative
     !x: value of the point
     !-------------------------------------
     
     integer, intent(in) :: n_col, ic, j
     real(DBL), intent(in) :: x
     complex(DBL), intent(in) :: ld 
     integer:: k, nx, nxcolloc, imax
     complex(DBL):: S

     S = 0.0d0

     k = j + 1
     nx = nx_array(ic)
     nxcolloc = n_col*(nx + 1) - 1
     imax = nxcolloc - 1
     

     if(k == n_col*nx) then
        
        S = compute_spline_val(n_col, k, x, xmesh(ic, 0:nx)) + &
             ld*compute_spline_val(n_col, k+1, x, xmesh(ic, 0:nx))
        
     end if
        
           
   end function Stilde_x



   function Stilde_p_x(n_col, ic, j, x, ld) result(S)
     !-------------------------------------
     !PURPOSE: 
     !Computes the value of \tilde{S}'_j(x)=S'_j(x)*ld*S'_{j+1}
     !INPUT:
     !n_col : 2 => cubic splines, 3 => quintic splines
     !ic: index of the channel
     !j : enumeration of the spline
     !x : value of the point
     !ld : logarithmic derivative
     !-------------------------------------
     
     integer, intent(in) :: n_col, ic, j
     real(DBL), intent(in) :: x
     complex(DBL), intent(in) :: ld
     integer:: k, nx, nxcolloc, imax
     complex(DBL):: S

     S = 0.0d0
     
     k = j + 1
     nx = nx_array(ic)
     nxcolloc = n_col*(nx + 1) - 1
     imax = nxcolloc - 1

     if(k == n_col*nx) then

        S =  compute_spline_1st_derivative(n_col, k, x, xmesh(ic, 0:nx)) + &
             ld*compute_spline_1st_derivative(n_col, k+1, x, xmesh(ic, 0:nx))        
     end if
        
           
   end function Stilde_p_x


   function Stilde_pp_x(n_col, ic, j, x, ld) result(S)
     !-------------------------------------
     !PURPOSE: 
     !Computes the value of \tilde{S}''_j(x)=S''_j(x)*ld*S''_{j+1}
     !INPUT:
     !n_col : 2 => cubic splines, 3 => quintic splines
     !ic: index of the channel
     !j : enumeration of the spline
     !x : value of the point
     !ld : logarithmic derivative
     !-------------------------------------
     
     integer, intent(in) :: n_col, ic, j
     real(DBL), intent(in) :: x
     complex(DBL), intent(in) :: ld
     integer:: k, nx, nxcolloc, imax
     complex(DBL):: S

     S = 0.0d0
     
     k = j + 1
     nx = nx_array(ic)
     nxcolloc = n_col*(nx + 1) - 1
     imax = nxcolloc - 1

     if(k == n_col*nx) then

        S =  compute_spline_2nd_derivative(n_col, k, x, xmesh(ic, 0:nx)) + &
             ld*compute_spline_2nd_derivative(n_col, k+1, x, xmesh(ic, 0:nx))
        
     end if
        
           
   end function Stilde_pp_x


   

   pure subroutine get_xlimits(n_col, ic, j, a, b)
     !-------------------------------------------------------
     !PURPOSE:
     !Returns the lower and upper limits of the spline S_j(x)
     !INPUT:
     !n_col : 2 => cubic splines, 3 => quintic splines
     !ic: index of the channel
     !j: index of the spline
     !OUTPUT:
     !a: lower limit
     !b: upper limit
     !-------------------------------------------------------
     
     integer, intent(in):: n_col, ic, j
     real(DBL), intent(out):: a, b
     integer:: nx, i, k

     nx = nx_array(ic)
     
     k = j + 1

     i = k/n_col
     
     a = xmesh(ic, max(i-1, 0))
     b = xmesh(ic, min(nx, i+1))

   end subroutine get_xlimits

   
 end module spline_mod_cubic_quintic
 
