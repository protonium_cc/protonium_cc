
program x_pot

  use pot
  
!  use bp_pot, only : V_NN_BP_reg
!  use potential_regularization, only : MCUT, NCUT

  integer :: S, J, L, LP, nr, ir
  real(8) :: rc, lambda, ra, r, h
  real(8), dimension(2, 2) :: V_obe, V_c
  
  real(8) ::  mp = 938.2796d0         !proton mass 
  real(8) ::  mn = 939.5654d0         !neutron mass 
  real(8) ::  mm = 763.0d0            !meson mass

  real(8) :: Delta, dm

  Delta = mp - mm
  dm = mn - mp
  
  h = 1.0d0
  nr = 500

  open(unit = 10, file='pot_full.dat')
  
  S = 0
  L = 0
  Lp = 0
  J = 0

  rc = 1.5d0
  lambda = 1.0d0
  ra = 0.210d0 !Annihilation radius

  
  do ir = 1, nr
     r = ir*h

     call pot_isospin(2, S, L, J, rc, rc, lambda, lambda, &
          ra, r, Delta, dm, 0, 1, V_obe)

     call pot_isospin(2, S, L, J, rc, rc, lambda, lambda, &
          ra, r, Delta, dm, 1, 0, V_c)
     
     write(10,"(5(2x,g13.6))") r, V_obe(1, 1), V_c(1, 1), (V_obe(1, 1)+V_c(1, 1)), V_obe(1, 2)

  end do
  
  close(10)

  
end program x_pot
