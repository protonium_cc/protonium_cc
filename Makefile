FC=gfortran
FFLAGS=-Wall -Wextra   -march=native  -pedantic -fimplicit-none -O3   -fopenmp  -m64  # -funroll-loops
BUILD_DIR=./
MOD_PATH=./
LIB_PATH=./
BIN_PATH=./
OBJ_SPLINES= spline_mod_1d.o x_splines.o
OBJ_SPLINES_2= spline_quintic.o x_spline_quintic.o
OBJ_SPLINES_3= spline_cubic_quintic.o x_spline_gen.o

OBJ = spline_cubic_quintic.o core_lib.o legendre_new.o protonium_cc_mod.o x_protonium_cc.o
OBJ2 = spline_cubic_quintic.o bstate_cc_wm_mod.o x_bstate_cc_wm.o
OBJ_LA = linalg.o x_test_linalg.o



#OBJ = spline_quintic.o bstate_cc_mod.o x_bstate_cc.o
#OBJ2 = spline_cubic.o bstate_cc_cubic_mod.o x_bstate_cc_cubic.o
OBJ3 = pot.o x_pot.o
OBJ4 = pot.o x_pot_nij.o

all: x_protonium_cc x_pot x_pot_nij

%.o: %.f95
	$(FC) -c $(FFLAGS) -I. -I$(BUILD_DIR) -I$(MOD_PATH) -J$(BUILD_DIR) $< -o $@

x_splines: $(OBJ_SPLINES)
	$(FC) $(FFLAGS) -o $@  $^ -lblas -llapack 

x_protonium_cc: $(OBJ)
	$(FC) $(FFLAGS) -o $@  $^ -lblas -llapack 

x_bstate_cc_wm: $(OBJ2)
	$(FC) $(FFLAGS) -o $@  $^ -lblas -llapack 

x_test_linalg: $(OBJ_LA)
	$(FC) $(FFLAGS) -o $@  $^



#x_bstate_cc_cubic: $(OBJ2)
#	$(FC) $(FFLAGS) -o $@  $^ -lblas -llapack 



x_spline_quintic: $(OBJ_SPLINES_2)
	$(FC) $(FFLAGS) -o $@  $^ -lblas -llapack 

x_spline_gen: $(OBJ_SPLINES_3)
	$(FC) $(FFLAGS) -o $@  $^  



x_pot : $(OBJ3)
	$(FC) $(FFLAGS) -o $@  $^ 	

x_pot_nij : $(OBJ4)
	$(FC) $(FFLAGS) -o $@  $^ 	




clean:
	rm -fv x_splines x_bstate_cc x_spline_quintic x_bstate_cc_cubic x_bp_pot x_spline_gen x_bstate_cc_wm
	rm -fv *.o
	rm -fv *.mod
	rm -fv *~
