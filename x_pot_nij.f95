
program x_pot

  use pot
  
!  use bp_pot, only : V_NN_BP_reg
!  use potential_regularization, only : MCUT, NCUT

  integer :: S, J, L, LP, nr, ir
  real(8) :: rc, lambda, ra, r, h
  real(8), dimension(2, 2) :: V_obe, V_c
  
  real(8) ::  mp = 938.2796d0         !proton mass 
  real(8) ::  mn = 939.5654d0         !neutron mass 
  real(8) ::  mm = 763.0d0            !meson mass

  real(8) :: Delta, dm, V_0, V_1

  Delta = mp - mm
  dm = mn - mp
  
  h = 0.01d0
  nr = 500

  open(unit = 10, file='pot_nij.dat')
  
  S = 0
  L = 0
  Lp = 0
  J = 0

  rc = 0.8d0
  lambda = 1.0d0
  ra = 0.210d0 !Annihilation radius

  
  do ir = 0, nr
     r = ir*h

     call V_NN_NIJ_reg(0, S, J, L, LP, 1, 3, rc, r, V_0)

     call V_NN_NIJ_reg(1, S, J, L, LP, 1, 3, rc, r, V_1)
     
     write(10,"(5(2x,g13.6))") r, V_0, V_1

  end do
  
  close(10)

  
end program x_pot
