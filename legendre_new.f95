module legendre_new

  integer, parameter, private::DBL=kind(1.0d0), IMAX = 1000
  real(DBL), parameter, private::EPS=1.0d-12
  real(DBL), parameter, private::PI=3.141592653589793238462643383279502884197169d0

contains

  function P_n(x,n) result(res)
    real(DBL), intent(in)::x
    integer, intent(in)::n
    real(DBL)::res 
    integer::k
    real(DBL)::P_m1, P_m2, P

    if(n==0) then
       res=1.0d0
    else if(n==1) then
       res=x
    else 
       P_m2=1.0d0
       P_m1=x
       P=0.0d0
       do k=2, n
          P=(1-1.0d0/k)*(x*P_m1-P_m2)+x*P_m1
          P_m2=P_m1
          P_m1=P
       end do
       res=P
    end if

  end function P_n


  function dP_n(x,n) result(res)
    real(DBL), intent(in)::x
    integer, intent(in)::n
    real(DBL)::res 

    res=(-n*x*P_n(x,n)+n*P_n(x,n-1))/(1-x**2)
  end function dP_n


  subroutine legendre_weights(nodes,weights,n)
    integer, intent(in)::n
    real(DBL), dimension(n), intent(in)::nodes 
    real(DBL), dimension(n), intent(inout)::weights
    integer::k

    do k=1, n
       weights(k)=2*(1-nodes(k)**2)/(n*P_n(nodes(k),n-1))**2
    end do

  end subroutine legendre_weights


  subroutine legendre_nodes(nodes,n)
    integer, intent(in)::n
    real(DBL), dimension(n), intent(inout)::nodes
    integer::k, j
    real(DBL)::theta_nk, P, dP, delta

    do k=0, n-1
       theta_nk=(n-k-0.5d0)*PI/(n)
       nodes(k+1)=cos(theta_nk)

       
       do j=1, IMAX
          P=P_n(nodes(k+1),n)
          dP=dP_n(nodes(k+1),n)
          delta=-P/dP
          nodes(k+1)=nodes(k+1)+delta

          if(abs(delta) < EPS) exit
          
       end do

       
       
 !      if (abs(diff)>EPS) then
 !         write(0,*) "Error calculation of nodes did not converge", diff
 !       end if
        

    end do
    

      
    
  end subroutine legendre_nodes




end module legendre_new
